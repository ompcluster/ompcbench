### Summary

<!-- Explain the problem you are trying to solve -->


### Proposed Schema

<!-- Provide a snippet of how the schema should look like -->

```yaml
# The new schema goes like this...
```

### Use cases

<!-- Provide some examples where the proposed schema is useful -->

Related Issues:

/label ~"To Do" ~"Schema"
