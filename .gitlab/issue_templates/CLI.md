### Summary

<!-- Explain the command or flag you wish to introduce -->

### Usage

<!-- Give and example of your command/flag in action -->

```console
$ ompcbench my-new-command --some-flag
This line is the output of the command above!
```

### Use cases

<!-- Provide some examples where your command is useful -->

Related Issues:

/label ~"To Do" ~"CLI"
