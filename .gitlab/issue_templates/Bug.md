### Summary

<!-- Summarize the problem you are facing -->

### Stack Trace

<!-- Provide the stack trace (if any) when the error occurs -->

```
# Stack trace goes here!
```

### Steps to reproduce

<!-- Provide the step-by-step instructions on how to reproduce this bug -->

### Configuration file

<!-- Paste the YAML configuration file you have used -->

```yaml
# YAML config goes here!
```

Related Issues:

/label ~"To Do" ~"Bug"
