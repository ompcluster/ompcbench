all: check

check: test check-format

test:
	pytest

check-format:
	black --check --diff .

.PHONY: all check test check-format
