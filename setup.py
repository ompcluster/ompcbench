#!/usr/bin/env python

import os
from setuptools import setup, find_packages


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(here, rel_path), "r") as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


with open("README.md", "r") as f:
    README = f.read()

with open("requirements/common.txt", "r") as f:
    common_requirements = f.readlines()

with open("requirements/dev.txt", "r") as f:
    dev_requirements = f.readlines()[1:]


# fmt: off
setup(

    # Metadata
    name = "ompcbench",
    version = get_version("ompcbench/__init__.py"),
    description = "A command-line tool for benchmarking OmpCluster",
    long_description = README,
    license = "MIT",
    author = "Gustavo Leite",
    author_email = "gustavo.leite@ic.unicamp.br",

    # Package configuration
    python_requires = ">=3.6, <4",
    packages = find_packages("."),
    project_urls = {
        "Bug Reports": "https://gitlab.com/ompcluster/ompcbench/issues",
        "Source": "https://gitlab.com/ompcluster/ompcbench"
    },

    # Entry points
    entry_points = {
        "console_scripts": [
            "ompcbench = ompcbench.__main__:main",
            "ompcb = ompcbench.__main__:main",
        ]
    },

    # Dependency management
    install_requires = common_requirements,
    extras_require = {
        "dev": dev_requirements,
    },
)
