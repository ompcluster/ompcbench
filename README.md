OMPC Bench
================================================================================

A command-line tool for benchmarking OmpCluster with TaskBench.

Overview
--------------------------------------------------------------------------------

OmpCluster Bench is capable of:

- Easily setup experiment methodology via YAML configuration files.
- Bulk launch Slurm jobs with varying parameters for benchmarking.
- Analyze the output of the Slurm jobs and generating a table.
- and more...

Installing
--------------------------------------------------------------------------------

In order to use OMPC Bench the user must install
[GraphViz](https://graphviz.org/) and some Python libraries as dependencies. To
install GraphViz, run one of the commands below depending on your environment:

```console
# Install it using your package manager of choice
$ sudo apt install graphviz

# Or, if you are working on a cluster, load the module
$ module load graphviz
```

Then, clone the repository the OMPC Bench repository and enter it. Run the
following commands to create a virtual environment:

```console
# Install virtualenv package
$ pip install --user virtualenv

# Create virtual environment
$ python3 -m virtualenv venv

# Activate virtual environment
$ source venv/bin/activate
```

Next, if you are a user looking to simply use OMPC Bench, run the following
command to install the common dependencies.

```console
$ pip install -e .
```

Or, if you are a developer, we recommend installing OMPC Bench with development
dependencies like so:

```console
$ pip install -e ".[dev]"
```

### Troubleshooting Installation

In some environments (e.g. the Santos Dumont cluster), OMPCBench can fail to
install due to a compilation error when building the `pygraphviz` library. In
that case, run the following commands in that order:

```console
$ module load graphviz
$ pip install pygraphviz --global-option=build_ext --global-option="-I/usr/include/graphviz"
$ pip install -e .
```

Usage
--------------------------------------------------------------------------------

In order to use OMPC Bench, you will need a configuration file that describes
your experiment. Consider the example below (also available as
a [template](config/taskbench.yaml)):

```yaml
# ------------------------------------------------------------------------------
# REQUIRED SECTIONS
# ------------------------------------------------------------------------------

# OMPC Bench required version
version: '1.0'

general:
    # Name of your experiment (optional, default=benchmark)
    name: my_cool_experiment
    # Information about TaskBench
    taskbench:
        # Path to TaskBench directory
        path: /path/to/taskbench
        # TaskBench git hash (optional, default=None)
        # No need to spell out the entire hash, only the first few characters.
        hash: abcdef12345

slurm:
    # Slurm account of the cluster
    account: slurm_acc
    # Slurm partition to submit jobs
    partition: slurm_part
    # Slurm reservation (optional, default="")
    reservation: slurm_res
    # Timelimit of Slurm jobs (optional, default=01:00:00)
    timelimit: 02:30:00
    # Verbosity of the Slurm script (optional, default=0)
    verbosity: 1
    # Flags to be passed to Slurm (optional, default=[])
    flags: ["--quiet"]

experiment:
    # Amount of samples to collect (optional, default=1)
    samples: 5
    # Matrix of options to be tested
    matrix:
        # Amount of nodes to allocate (including the head node)
        nodes: [2, 3, 5]
        # TaskBench kernels
        kernel: ['compute_bound', 'compute_dgemm']
        # Task graph size (formatted as [width, steps])
        size: [[16, 32], [128, 256]]
        # Number of iterations per task
        iter: [10_000, 100_000, 1_000_000]
        # Computation to communication ratio of tasks
        CCR: [0.5, 1.0, 2.0]
        # Dependency type between tasks
        dep: ['trivial', 'fft', 'stencil_1d']
        # Strategies your want to test
        # Each strategy that you define will be associated with an environment.
        # See section 'environment' below.
        strat: ['baseline', 'optimized']

# ------------------------------------------------------------------------------
# OPTIONAL SECTIONS
# ------------------------------------------------------------------------------

environment:
    # Environment variables set for all jobs
    all:
        # Environment Modules (optional)
        modules: ['module1', 'module2']
        # Environment variables (optional)
        variables:
            LIBOMPTARGET_DEBUG: 1
            LIBOMPTARGET_INFO: 1
            # Example of variable substitution (see section `Variable Substitution`)
            OMPCLUSTER_PROFILE: "profile/{strat}-{dep}-{iter}-{CCR}"
            OMPCLUSTER_TASK_GRAPH_DUMP_PATH: "dot/{strat}-{dep}-{iter}-{CCR}"
        # Container image (optional, may be overwritten by strategies)
        image:
            # Path to Singularity image
            path: /path/to/all_container.sif
            # OmpCluster git hash (optional, default=None)
            ompc_hash: 12345abcdef
            # Singularity flags (optional, default=[])
            flags: ['--foo', '--bar']

    # Environment variables set for jobs with strategy baseline
    baseline:
        # TaskBench settings (optional)
        taskbench:
            # TaskBench binary relative to 'general.taskbench.path'.
            # (optional, default=ompcluster/main)
            binary: ompcluster/main
            # TaskBench flags (optional, default=[])
            flags: ["flag_1 value", "flag_2"]
        # Environment Modules (optional, merged with `environment.all.modules`)
        modules: ['baseline1', 'baseline2']
        # Environment variables (optional)
        variables:
            OMPCLUSTER_SCHEDULER: roundrobin

    # Environment variables set for jobs with optimized strategy
    optimized:
        # TaskBench settings (optional)
        taskbench:
            # TaskBench binary relative to 'general.taskbench.path'.
            # (optional, default=ompcluster/main)
            binary: ompcluster/main_with_optimization
            # TaskBench flags (optional, default=[])
            flags: ["flag_1 value", "flag_2"]
        # Environment Modules (optional, merged with `environment.all.modules`)
        modules: ['opt1', 'opt2']
        # Environment variables (optional)
        variables:
            OMPCLUSTER_SCHEDULER: heft
        # Container image (optional, overwrites `environment.all.image`)
        image:
            # Path to Singularity image
            path: /path/to/optimized_container.sif
            # OmpCluster git hash (optional, default=None)
            ompc_hash: 12345abcdef
            # Singularity flags (optional, default=[])
            flags: []
```

After filling all the information carefully and saving it to a file (e.g.
`experiment.yaml`), check if your configuration file is valid through the
command:

```console
$ ompcbench check experiment.yaml
```

If it is valid, then submit all the jobs to execution with Slurm:

```console
$ ompcbench run experiment.yaml
```

You can also divide your configuration into multiple files and pass them all
through the command-line like the example below. This repository provides
partial configuration for the most common clusters (Sorgan, SDumont, Ogbon) so
you don't have to write them yourself. Keep in mind that the "union" of all
those files must pass the check.

```console
$ ompcbench check config/sorgan.yaml experiment.yaml
$ ompcbench run config/sorgan.yaml experiment.yaml
```

After all of your jobs finish, you can summarize the results with the `analyze`
command, like so:

```console
$ ompcbench analyze /path/to/experiment_000
```

Check the help pages for more information about options and flags:

```console
$ ompcbench --help
$ ompcbench check --help
$ ompcbench run --help
$ ompcbench analyze --help
```

Variable Substitution
--------------------------------------------------------------------------------

When setting environment variables, it may be desirable to use information about
the current job to set a proper value for the environment variable. One such
example is collecting traces with OmpCluster. In order to produce a trace file,
the user must set the `OMPCLUSTER_PROFILE` to a path where the traces will be
stored. If the same path is used across multiple invocations, the trace files
are overwritten at each invocation. This is particularly problematic when using
OMPCBench, where the user would like to launch hundreds of jobs at once.
Therefore, one solution is to use variable substitution to assign a unique name
for each invocation. Take the following snippet as example:

```yaml
OMPCLUSTER_PROFILE: "profile/{strat}-{dep}-{iter}-{CCR}"
```

In this case, the trace for a particular invocation will be stored inside
`profile`, relative to the TaskBench directory. On top of that, the syntax
`{varname}` provides a mechanism to use information about the current job being
launched to set the environment variable. Possible values for `varname` are:

- `nodes`: how many nodes (including head node) this job will request;
- `kernel`: which TaskBench kernel is being used;
- `iter`: how many iterations are executed for each kernel;
- `width`: how wide is the task graph;
- `steps`: how tall is the task graph;
- `CCR`: the computation to communication ratio;
- `dep`: the dependency type;
- `strat`: the strategy;
- `output`: the amount of output bytes produced by each task;
- **Environment variables**: it is also possible to use other environment
  variables, *i.e.* the current working directory, such as `OMPCLUSTER_PROFILE:
  {PWD}/trace`.

Compatibility
--------------------------------------------------------------------------------

The schema of OMPC Bench configuration files _can_ and _will_ be changed in the
future if new requirements appear, breaking compatibility with existing
configuration files. The following versioning system was adopted
`major.minor.patch` to keep things sane, such that the minor version of OMPC
Bench will be increased each time a schema change is made. The patch number will
be increased in every new merge request.

Contributing
--------------------------------------------------------------------------------

OMPC Bench is still a work in progress. Please send patches and suggestion
through [Gitlab](https://gitlab.com/ompcluster/ompcbench/-/issues).
