from numpy import average, median
import orjson
import pygraphviz as pgv
import pandas as pd

from ompcbench.util import (
    get_dir_list,
    get_output_prefix_suffix,
    get_lower_ts,
    get_nearest_evt,
    get_mpi_tag,
    PUSH_NEW_EVT,
    TASK_EVT,
    TARGET_EVTS,
    SCHEDULE_EVT,
    NUM_DECIMALS,
)

from ompcbench.critical_path import (
    get_schedule_evt,
    get_push_evts,
    get_task_evts_by_id,
    calculate_critical_path,
)


def get_formatted_critical_path_metric(count, value):
    """Returns the critical path metric in COUNT x METRIC format."""
    return str(count) + " x " + str(value)


def calculate_critical_path_metrics(
    name, load_balances, longest_path, all_critical_path_metrics
):
    """
    Calculates the summarized critical path metrics and adds it to the metrics
    dictionary. These metrics help provide an interpretation of load balances.
    """
    # Get the maximum and minimum load balance without the critical path value
    load_balances_wo_critical_path = [lb for lb in load_balances if lb != 1.0]
    max_load_balance = max(load_balances_wo_critical_path)
    min_load_balance = min(load_balances_wo_critical_path)

    # Calculates the metrics
    all_critical_path_metrics["Name"].append(name)
    all_critical_path_metrics["Critical Path Time [ms]"].append(
        get_formatted_critical_path_metric(
            load_balances.count(1.0), round(longest_path, NUM_DECIMALS)
        )
    )
    all_critical_path_metrics["Max Critical Path Metric"].append(
        get_formatted_critical_path_metric(
            load_balances.count(max_load_balance), max_load_balance
        )
    )
    all_critical_path_metrics["Min Critical Path Metric"].append(
        get_formatted_critical_path_metric(
            load_balances.count(min_load_balance), min_load_balance
        )
    )
    all_critical_path_metrics["Average"].append(round(average(load_balances), 2))
    all_critical_path_metrics["Median"].append(round(median(load_balances), 2))


def get_critical_path_simple_metrics(directory, trace_files):
    """Returns  from a single OMPC execution."""
    pid_head_process = -1
    # Task-related event lists
    target_evts = []
    push_evts = []
    schedule_evts = []
    execute_evts = []

    # Get trace events and synchronize them
    for tracing_file in trace_files:
        tracing = open(tracing_file, "r").read()
        # Convert trace to JSON objects
        objs = orjson.loads(tracing)

        # Get lower ts to synchronize events
        lower_ts = get_lower_ts(objs["traceEvents"])

        for evt in objs["traceEvents"]:
            # Get task-related events
            if evt["ph"] == "X":
                evt["ts"] = evt["ts"] - lower_ts
                if evt["name"] == TASK_EVT:
                    execute_evts.append(evt)
                elif evt["name"] in TARGET_EVTS:
                    target_evts.append(evt)
                elif evt["name"] == SCHEDULE_EVT:
                    schedule_evts.append(evt)
                elif evt["name"] == PUSH_NEW_EVT:
                    push_evts.append(evt)
            # Get events that name processes and threads
            elif evt["ph"] == "M" and evt["name"] != "process_sort_index":
                # Get head process pid
                if evt["name"] == "process_name":
                    if evt["args"]["name"] == "Head Process":
                        pid_head_process = evt["pid"]

    # Relates execute event MPI Tag to task id
    task_ids = {}
    for target_evt in target_evts:
        # Get schedule event related to a target event
        schedule_evt = get_schedule_evt(target_evt, schedule_evts)

        # Get push events related to a target event
        push_exec_evts = get_push_evts(target_evt, push_evts, ["Execute"])
        # Get executed event related to a push event
        push_exec_evt = get_nearest_evt(target_evt, push_exec_evts)

        # Relates MPI Tag to a task id
        mpi_tag = get_mpi_tag(push_exec_evt)
        task_ids[mpi_tag] = schedule_evt["args"]["task_id"]

    # Relates task id to execute target events
    task_evts = get_task_evts_by_id(execute_evts, task_ids, pid_head_process)

    for graph_file in directory.glob("*.dot"):
        graph = pgv.AGraph(graph_file, strict=False, directed=True)
        # Ensure the color of the edges is different from CRITICAL_PATH_COLOR
        graph.edge_attr.update(color="black")

        # Delete nodes that are not related to an Execute Target event
        for n in graph.nodes():
            if n not in task_evts:
                graph.delete_node(n)
        # Get the root nodes to calculate the critical path from them
        root_nodes = []
        for n in graph.nodes_iter():
            if not graph.predecessors(n):
                root_nodes.append(n)

        # Stores critical path time for each graph node
        path_time = {}
        # Longest critical path time of all root nodes
        longest_path = -1
        # Id of the root node with the longest time
        for root_node in root_nodes:
            calculate_critical_path(root_node, path_time, task_evts, graph)
            # Check if the current root node has a longer critical path
            if path_time[root_node] > longest_path:
                longest_path = path_time[root_node]

        # Sort path times
        path_time_sorted = dict(sorted(path_time.items(), key=lambda item: item[1]))

        # Getting the nodes and path times separately
        path_time_values = list(path_time_sorted.values())
        path_time_nodes = list(path_time_sorted.keys())

        # Calculate the load balance for each root node
        load_balances = []
        root_nodes_path_times = []
        for i in range(len(path_time_values)):
            path_time_value = path_time_values[i]
            node = path_time_nodes[i]
            if node in root_nodes:
                load_balances.append(round(path_time_value / longest_path, 2))
                root_nodes_path_times.append(path_time_value)

        critical_path_simple_metrics = {
            "Root Nodes": root_nodes,
            "Load Balances": load_balances,
            "Path Times": root_nodes_path_times,
        }

        return critical_path_simple_metrics, longest_path


def extract_critical_path_metrics(output, root_dir, trace_files=None):
    """
    Extract the critical path metrics from multiple OMPC executions divided into
    separate folders.
    """

    # Get list of directories to extract metrics separately
    if trace_files is None:
        # Each folder has a different OMPCluster execution
        dir_list = get_dir_list(root_dir)
    else:
        # Only one folder
        dir_list = [root_dir]

    # Separate the output suffix and prefix
    output_prefix, output_suffix = get_output_prefix_suffix(output)

    # All summarized critical path metrics
    all_critical_path_metrics = {
        "Name": [],
        "Critical Path Time [ms]": [],
        "Max Critical Path Metric": [],
        "Min Critical Path Metric": [],
        "Average": [],
        "Median": [],
    }

    for directory in dir_list:
        # Get trace files when there are multiple OMPC executions
        if trace_files is None:
            trace_files = directory.glob("*.json")

        # Get critical path metrics in a simplified format
        critical_path_simple_metrics, longest_path = get_critical_path_simple_metrics(
            directory, trace_files
        )

        # Each directory will have a different file for simplified metrics
        critical_path_simple_metrics_df = pd.DataFrame(critical_path_simple_metrics)
        critical_path_simple_metrics_df.to_csv(
            output_prefix + directory.name + output_suffix, index=False
        )

        # Calculates summarized matrics and adds all_critical_path_metrics
        calculate_critical_path_metrics(
            directory.name,
            critical_path_simple_metrics["Load Balances"],
            longest_path,
            all_critical_path_metrics,
        )

    # Pandas DataFrame with all metrics
    pd.set_option("display.precision", NUM_DECIMALS)
    all_critical_path_metrics_df = pd.DataFrame(all_critical_path_metrics)

    # Sort metrics by application name
    all_critical_path_metrics_df = all_critical_path_metrics_df.sort_values(by=["Name"])

    # Write results
    all_critical_path_metrics_df.to_csv(output, index=False)
