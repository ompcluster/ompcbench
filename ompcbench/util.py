import os
import subprocess as sp
import glob
import sys

from pathlib import Path


class Map(dict):
    """Access a dictionary using the dot notation."""

    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


class Counter(dict):
    """Counts hashable objects, starting at 1."""

    def get(self, key):
        self[key] += 1
        return self[key]

    def __missing__(self, key):
        self[key] = 0
        return 0


def run_cmd(args, cwd=None, env={}):
    """Run a command in a separate process and pipe its output."""

    # Merge the environment built for the experiment with current process
    # environment, otherwise the script will not have basic variables like PATH
    # and LIBRARY_PATH set.
    penv = {**os.environ, **env}

    try:
        # Run another process and capture its output
        proc = sp.run(args, capture_output=True, cwd=cwd, env=penv)
        return Map(
            {
                "code": proc.returncode,
                "stdout": proc.stdout,
                "stderr": proc.stderr,
            }
        )
    except Exception as error:
        return Map(
            {
                "code": -1,
                "stdout": "",
                "stderr": str(error),
            }
        )


def check_files(prefix):
    """
    Function to check if dir and prefix exist.
    """
    if os.path.dirname(prefix) != "":
        if not os.path.isdir(os.path.dirname(prefix)):
            sys.exit("Error: " + os.path.dirname(prefix) + " is not a directory.")
    if not glob.glob(prefix + "*"):
        sys.exit("Error: " + prefix + " does not match any file.")


# Events names and lists
# ==============================================================================

TARGET_EVTS = [
    "__tgt_target_nowait_mapper",
    "__tgt_target_kernel_nowait",
    "__tgt_target_kernel",
]

TARGET_BEGIN_EVTS = [
    "__tgt_target_data_begin_nowait_mapper",
]

TARGET_END_EVTS = [
    "__tgt_target_data_end_nowait_mapper",
]

TASK_EVT = "Execute / Target Execution"
EXCHANGE_BEGIN_EVT = "Exchange / Begin"
EXCHANGE_END_EVT = "Exchange / End"
SUBMIT_BEGIN_EVT = "Submit / Begin"
SUBMIT_END_EVT = "Submit / End"
RETRIEVE_BEGIN_EVT = "Retrieve / Begin"
RETRIEVE_END_EVT = "Retrieve / End"
PUSH_NEW_EVT = "pushNewEvent"
SCHEDULE_EVT = "Schedule Task"

COMM_EVTS = [
    EXCHANGE_BEGIN_EVT,
    EXCHANGE_END_EVT,
    SUBMIT_BEGIN_EVT,
    SUBMIT_END_EVT,
    RETRIEVE_BEGIN_EVT,
    RETRIEVE_END_EVT,
]

PUSH_COMM_EVTS = ["Exchange", "Submit", "Retrieve"]

NUM_DECIMALS = 4

# Events checking functions
# ==============================================================================


def is_from_same_thread(first_evt, sec_evt):
    """
    Returns True if the second event and first event have the same process id
    (pid) and thread id (tid) and False otherwise.
    """
    return first_evt["pid"] == sec_evt["pid"] and first_evt["tid"] == sec_evt["tid"]


def is_inner_event(main_evt, inner_evt):
    """
    Returns True if the event duration is inner to the main event duration and
    False otherwise.
    """
    return (
        is_from_same_thread(main_evt, inner_evt)
        and (main_evt["ts"] < inner_evt["ts"])
        and ((main_evt["ts"] + main_evt["dur"]) > (inner_evt["ts"] + inner_evt["dur"]))
    )


# Time functions
# ==============================================================================


def get_lower_ts(evts):
    """Returns the lowest begin time of non-metadata events."""
    return (
        min(
            list(filter(lambda evt: evt["ph"] == "X", evts)),
            key=lambda evt: evt["ts"],
        )
    )["ts"]


def get_finalization_time(evts):
    """Returns the greatest finalization time of non-metadata events."""
    finalization_evt = max(
        list(filter(lambda evt: evt["ph"] == "X", evts)),
        key=lambda evt: evt["ts"] + evt["dur"],
    )
    return finalization_evt["ts"] + finalization_evt["dur"]


# Functions to get common properties
# ==============================================================================


def get_dir_list(root_dir):
    """Returns the list of directories that are on a given path."""
    # Each folder has a different execution
    root_dir_path = Path(root_dir)
    dir_list = []
    for obj in root_dir_path.iterdir():
        if obj.is_dir():
            dir_list.append(obj)

    # Check if there is only one execution
    if len(dir_list) == 0:
        dir_list = [root_dir_path]

    return dir_list


def get_output_prefix_suffix(output, default_suffix=".csv"):
    """
    Returns the name of the output file separated by suffix and prefix. If the
    file has multiple suffixes, the suffix is the concatenation of all of them.
    """
    if "." in output:
        output_splitted = output.split(".")
        output_prefix = output_splitted[0]
        output_suffix = output[len(output_prefix) :]
    else:
        output_suffix = default_suffix
        output_prefix = output
    output_prefix = output_prefix + "_"
    return output_prefix, output_suffix


def get_mpi_tag(evt):
    """Returns the MPI Tag checking the different possible formats."""
    if "MPI Tag" in evt["args"]:
        return evt["args"]["MPI Tag"]
    else:
        return evt["args"]["mpi_tag"]


def get_name_prefix(evt):
    """Returns the name prefix of events of type Begin, End or Wait."""
    return evt["name"].split(" / ")[0]


# Events filtering functions
# ==============================================================================


def get_begin_evts(evts):
    """Returns a list of events that contain 'Begin' in the name."""
    return [e for e in evts if "Begin" in e["name"]]


def get_end_evts(evts, begin_evt=None):
    """
    Returns a list of events that contain 'End' in the name.
    If begin_evt was given, only events started after it will be returned.
    """
    if begin_evt:
        return [e for e in evts if "End" in e["name"] and e["ts"] > begin_evt["ts"]]
    else:
        return [e for e in evts if "End" in e["name"]]


def get_evts_by_name(name, evts):
    """Returns a list of events with the same given name."""
    return [e for e in evts if e["name"] == name]


def get_evts_by_name_prefix(name_prefix, evts):
    """Returns a list of events with the same given name prefix."""
    return [e for e in evts if name_prefix in e["name"]]


def get_evts_same_mpi_tag(main_evt, evts):
    """Returns a list of events with the same MPI Tag as main event."""
    return [e for e in evts if get_mpi_tag(main_evt) == get_mpi_tag(e)]


def get_evts_same_name_prefix(main_evt, evts):
    """Returns a list of events with the same name prefix as the main event."""
    return [e for e in evts if get_name_prefix(main_evt) == get_name_prefix(e)]


def get_evts_same_thread(main_evt, evts):
    """Returns a list of events with the same pid and tid as main event."""
    return [e for e in evts if is_from_same_thread(main_evt, e)]


def get_push_assoc_evts(push_evt, assoc_evts):
    """
    Returns a list of events associated with push.
    These events must have the same name as the push event_type and MPI Tag.
    """
    assoc_evts_same_tag = get_evts_same_mpi_tag(push_evt, assoc_evts)
    return [
        e for e in assoc_evts_same_tag if push_evt["args"]["event_type"] in e["name"]
    ]


""" 
Indicates if the get_nearest_evt function needs to get the event nearest to the 
begin or end of the main event.
"""
NEAREST_TO_BEGIN = 1
NEAREST_TO_END = 0


def get_nearest_evt(main_evt, evts, nearest_to_begin=NEAREST_TO_BEGIN):
    """Return the nearest event to the main event."""
    if not evts:
        return None
    if nearest_to_begin:
        return min(
            evts,
            key=lambda evt: abs(evt["ts"] - main_evt["ts"]),
        )
    else:
        return min(
            evts,
            key=lambda evt: abs(
                evt["ts"] + evt["dur"] - main_evt["ts"] - main_evt["dur"]
            ),
        )


def get_inner_evts(main_evt, evts):
    """Returns all inner events of a main event."""
    evts_same_thread = get_evts_same_thread(main_evt, evts)
    if not evts_same_thread:
        return None
    inner_evts = [e for e in evts_same_thread if is_inner_event(main_evt, e)]
    if not inner_evts:
        return None
    return inner_evts


def get_inner_evt(main_evt, evts):
    """Returns the nearest inner event to a main event."""
    inner_evts = get_inner_evts(main_evt, evts)
    if inner_evts is None:
        return None
    return get_nearest_evt(main_evt, inner_evts)


# Dependency functions
# ==============================================================================

# Indicates if the edge event represents the begin or end point of the
# dependency on the timeline.
BEGIN_EVT = 1
END_EVT = 0

# Indicates if the edge event time point should be the begin or end of the
# associated event.
TIME_AS_BEGIN = 1
TIME_AS_END = 0


def create_edge_evt(edge_evt_att, assoc_evt, evt_type, set_begin_time):
    """
    Returns an edge event associated with an event.
    - The edge event returned can have attributes provided in the edge_evt_att
    parameter.
    - Each dependency needs two edge events, chosen by the evt_type parameter: one
    to indicate when the dependency begins and one to indicate when it ends.
    - The begin or end point of a dependency can be located at the begin or end of
    the associated event (set_begin_time parameter).
    """

    # Create event
    edge_evt = {}

    # Set edge event common properties
    edge_evt.update(edge_evt_att)
    edge_evt["pid"] = assoc_evt["pid"]
    edge_evt["tid"] = assoc_evt["tid"]

    # Set edge attributes
    if evt_type == END_EVT:
        # Set end point dependence attributes
        edge_evt["ph"] = "f"
        edge_evt["bp"] = "e"

        # Set location on the timeline
        if set_begin_time:
            edge_evt["ts"] = assoc_evt["ts"]
        else:
            # Edge time should start just before the end time of the task event
            edge_evt["ts"] = (
                assoc_evt["ts"] + assoc_evt["dur"] - 0.000001 * assoc_evt["dur"]
            )
    elif evt_type == BEGIN_EVT:
        # Set begin point dependence attributes
        edge_evt["ph"] = "s"

        # Set location on the timeline
        if set_begin_time:
            edge_evt["ts"] = assoc_evt["ts"]
        else:
            # Edge time should start just before the end time of the task event
            edge_evt["ts"] = (
                assoc_evt["ts"] + assoc_evt["dur"] - 0.000001 * assoc_evt["dur"]
            )

    return edge_evt


def create_id(name, evt=None, hash_len=6):
    """
    Returns a identifier composed of a name and, optionally, an MPI Tag of the
    event.
    - This identifier is used as the id attribute of edge events.
    - The identifier len can be provided.
    """
    if evt is not None:
        return str(hash(name + str(get_mpi_tag(evt))))[1:hash_len]
    else:
        return str(hash(name))[1:hash_len]


def create_edge_dict(cat, id=None):
    """
    Create an edge dict with base info by passing the cat and id attribute.
    """
    return {
        "name": "edge",
        "cat": cat,
        "args": {},
        "id": id,
    }


def get_task_node_name(task_id):
    """Return the graph node name related to a task id."""
    return "V" + str(task_id)


def get_assoc_end_evt(begin_evt, end_evts):
    """Returns an end event associated with a given begin event."""
    # End event must: start after begin event, and have the same MPI Tag and the
    # same name prefix
    assoc_end_evts = [
        e
        for e in end_evts
        if e["ts"] > begin_evt["ts"]
        and get_mpi_tag(begin_evt) == get_mpi_tag(e)
        and get_name_prefix(begin_evt) == get_name_prefix(e)
    ]
    if len(assoc_end_evts) > 0:
        # Get the nearest end event
        return get_nearest_evt(begin_evt, assoc_end_evts)
    else:
        # There is no associated end event
        return None
