import yaml

from ompcbench import messages as msg
from ompcbench.util import Map

# The top-level directory where the results will be saved by default
RESULTS_DIR = "results"


class Parser(object):
    """Parses content from YAML file."""

    __dummy_options = Map({})

    def __init__(self, config, options=__dummy_options):
        # Command line options
        self.options = options
        # Configuration (from YAML file)
        self.config = config
        # Path where output will be saved
        self.output = options.output

        # Get the application used
        if config.experiment.plasma.samples:
            self.type = "plasma"
        elif config.experiment.taskbench.samples:
            self.type = "taskbench"
        elif config.experiment.blockmatmul.samples:
            self.type = "blockmatmul"

        # Details of the experiment
        self.name = config.name
        self.keys = config.experiment[self.type].matrix.keys()
        self.vals = config.experiment[self.type].matrix.values()

        # Details of the launcher used
        # TODO: Use generalized launcher
        self.slurm = config.slurm
        self.verbosity = config.slurm.verbosity
        # Amount of samples to collect
        self.samples = config.experiment[self.type].samples
        # Dictionary that hold the logfiles
        self.logs = Map(
            {
                "launch": None,
                "commands": None,
            }
        )
        # Collection of environment variables
        self.environment = config.environment

        # Details of the application used
        self.dir = config.experiment[self.type].path
        self.hash = config.experiment[self.type].hash


def log_launch(logfile, case, stdout):
    """Record the Slurm job ID of the current case."""
    # Get the job number from sbatch output.
    try:
        jobid = stdout.split()[-1].decode("utf-8")
    except IndexError:
        jobid = -1

    # FIXME: maybe only the values are enough here.
    for key, val in case.items():
        print(f"{key}={val}, ", end="", file=logfile)
    print(f"jobid={jobid}", file=logfile)


def display_header(parser, njobs):
    """Display experiment summary in stdout."""
    T = msg.table_writer(bold=True, fg="cyan")
    T("Experiment", parser.name)
    T("Output", parser.output)
    T("Application", parser.dir)
    if parser.hash:
        T(f"App Hash", parser.hash)
    T(f"Job Count", njobs)


def display_cmd(cmd, env, index):
    """Dump verbose information about the jobs to be launched."""
    msg.info(f"Command {index}: " + msg.style(" ".join(cmd), dim=True))
    msg.info(f"Environment:")

    for key, val in env.items():
        msg.info(f'- {key}="{val}"')
