import sys
import json
import pandas as pd
import numpy as np

# Global constants
# ==============================================================================

# The type of each column in a Trace's DataFrame
# Pandas usually get the data type wrong when loading from JSON.
# FIXME: This is currently unused.
COLUMN_TYPES = {
    "ph": "category",
    "name": "string",
    "pid": "int32",
    "tid": "int32",
    "arg_Destination": "int32",
    "arg_Location": "category",
    "arg_MPI_Tag": "int32",
    "arg_Origin": "int32",
    "arg_Name": "string",
    "arg_Device": "int32",
    "arg_Queued": "int32",
    "arg_Type": "object",
    "arg_Current_Task": "int32",
    "arg_Remaining_Tasks": "int32",
    "arg_name": "string",
    "arg_sort_index": "int32",
    "arg_target_region_index": "int32",
}

# Helper classes
# ==============================================================================


class Process:
    """Data class containing information about a process from a trace."""

    def __init__(self, pid: int, name: str, index: int = 0):
        self._pid = int(pid)
        self._name = name
        self._index = index
        self._is_head = name.startswith("Head")
        self._is_worker = ~self._is_head

    @property
    def pid(self):
        return self._pid

    @property
    def name(self):
        return self._name

    @property
    def index(self):
        return self._index

    @property
    def is_head(self):
        return self._is_head

    @property
    def is_worker(self):
        return self._is_worker

    @property
    def type(self):
        return "Head" if self.is_head else "Worker"

    def __repr__(self):
        return f"Process(name='{self.name}', pid={self.pid}, index={self.index})"


class Thread:
    """Data class containing information about a thread from a trace."""

    def __init__(self, tid: int, name: str, parent=None, index: int = 0):
        self._tid = int(tid)
        self._name = name
        self._index = index
        self._is_gate = name.startswith("Gate")
        self._is_handler = name.startswith("Event Handler")
        self._is_control = name.startswith("Control")
        self._is_worker = name.startswith("Worker")
        self._parent = parent

    @property
    def tid(self):
        return self._tid

    @property
    def name(self):
        return self._name

    @property
    def index(self):
        return self._index

    @property
    def is_gate(self):
        return self._is_gate

    @property
    def is_handler(self):
        return self._is_handler

    @property
    def is_control(self):
        return self._is_control

    @property
    def is_worker(self):
        return self._is_worker

    @property
    def parent(self):
        return self._parent

    @property
    def type(self):
        if self.is_control:
            return "Control"
        elif self.is_worker:
            return "Worker"
        elif self.is_gate:
            return "Gate"
        elif self.is_handler:
            return "Handler"

    def __repr__(self):
        return f"Thread(name='{self.name}', tid={self.tid})"


# Trace class
# ==============================================================================


class Trace(pd.DataFrame):
    """Wrapper class around Pandas DataFrame representing a JSON trace."""

    @classmethod
    def from_file(cls, file):
        """Create a trace from an opened file."""
        return Trace.from_json(json.load(file))

    @classmethod
    def from_str(cls, string):
        """Create a trace from a JSON string."""
        return Trace.from_json(json.loads(string))

    @classmethod
    def from_json(cls, obj):
        """Create a trace from a JSON (dictionary) object."""
        return Trace.from_dataframe(pd.json_normalize(obj["traceEvents"]))

    @classmethod
    def from_dataframe(cls, dataframe):
        """Create a trace from a Pandas DataFrame object.

        Some pre-processing is performed in this method to keep traces
        standardized.
        """

        df = dataframe.copy().rename(
            columns={
                col: col.replace("args.", "arg_").replace(" ", "_")
                for col in dataframe.columns
                if col.startswith("args.")
            }
        )

        # Remove NaNs and convert time units from us to ms
        df["ts"] = df["ts"].fillna(0) / 1000.0
        df["dur"] = df["dur"].fillna(0) / 1000.0

        # Create new column with event end times for convenience
        df["end"] = df["ts"] + df["dur"]

        return Trace(df)

    def __init__(self, *args, **kwargs):
        """Constructor."""
        super().__init__(*args, **kwargs)
        self._procs = None
        self._threads = None
        self._meta = None
        self.__build_metadata()

    def __build_metadata(self):
        """Eagerly compute some structures that may be useful later."""
        # Build DataFrame with metadata events only
        meta = self[self["ph"] == "M"]
        procs = {}
        threads = {}

        # Create dictionary of processes indexed by id
        for pid, df in meta[meta.name == "process_name"].groupby("pid"):
            procs[pid] = Process(pid, df.arg_name.str.cat())

        # Create dictionary of threads indexed by id
        for tid, df in meta[meta.name == "thread_name"].groupby("tid"):
            threads[tid] = Thread(tid, df.arg_name.str.cat(), procs[df.pid.iloc[0]])

        self._meta = meta
        self._procs = procs
        self._threads = threads

    @property
    def metadata(self):
        """Get only metadata information from the trace."""
        if self._meta is None:
            self._meta = self[self["ph"] == "M"]
        return self._meta

    def process(self, pid):
        """Query a process name given their ID."""
        return self._procs[pid]

    def thread(self, tid):
        """Query a thread name given their ID."""
        return self._threads[tid]

    @property
    def processes(self):
        """Yields events related to each process at a time."""
        for pid, df in self.groupby("pid"):
            yield self._procs[pid], Trace(df)

    @property
    def threads(self):
        """Yields events related to each thread at a time."""
        for tid, df in self.groupby("tid"):
            yield self._threads[tid], df

    @property
    def nprocs(self):
        """Return the number of processes in this trace."""
        return self.pid.nunique()

    @property
    def nthreads(self):
        """Return the number of threads in this trace."""
        return self.tid.nunique()
