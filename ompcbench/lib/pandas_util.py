import sys
import pandas as pd

from pathlib import Path

import ompcbench.messages as msg


def save_to_csv(dataframe, file, **kwargs):
    """Save DataFrame to CSV file."""
    return dataframe.to_csv(file, **kwargs)


def save_to_txt(dataframe, file, **kwargs):
    """Save DataFrame to text file."""
    print(string := dataframe.to_string(None, **kwargs), file=file)
    return string


def save_to_pkl(dataframe, file, **kwargs):
    """Save DataFrame to a Pickle file."""
    if file is None:
        msg.fatal("Cannot output pickle to stdout.")
    return dataframe.to_pickle(file.name, **kwargs)


def save_to_html(dataframe, file, **kwargs):
    """Save DataFrame to an HTML file."""
    return dataframe.to_html(file, **kwargs)


def save_to_latex(dataframe, file, **kwargs):
    """Save DataFrame to a LaTeX file."""
    return dataframe.to_latex(file, **kwargs)


def save_to_json(dataframe, file, **kwargs):
    """Save DataFrame to a JSON file."""
    # FIXME: Use better defaults
    return dataframe.to_json(file, **kwargs)


def save_to_md(dataframe, file, **kwargs):
    """Save DataFrame to a markdown file."""
    return dataframe.to_markdown(file, **kwargs)


# Dictionary that maps supported output formats to their functions
FORMATS = {
    "txt": save_to_txt,
    "csv": save_to_csv,
    "json": save_to_json,
    "pkl": save_to_pkl,
    "html": save_to_html,
    "tex": save_to_latex,
    # "md": save_to_md,
}

DEFAULT_KWARGS = {
    "txt": {"float_format": "%.2f", "na_rep": "-"},
    "csv": {},
    "json": {"indent": 2, "orient": "index"},
    "pkl": {},
    "html": {},
    "tex": {},
}


def save_dataframe(dataframe, *, file=sys.stdout, fmt="txt", **kwargs):
    """
    Save a DataFrame to a file with a specified format.

    If `file` is None, then return the contents in a string.
    """
    if file is not sys.stdout and hasattr(file, "name") and file.name:
        fmt = Path(file.name).suffix[1:]

    if fmt not in FORMATS.keys():
        msg.fatal(f"Unexpeced format `{fmt}`")

    # Get the keyword arguments to use for this file
    kwargs = DEFAULT_KWARGS[fmt] | kwargs.get(fmt, {})
    # Invoke  the proper formatting function and return its result
    return FORMATS[fmt](dataframe, file, **kwargs)
