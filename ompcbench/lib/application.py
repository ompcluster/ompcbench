import re
import os
import yaml
import pandas as pd

from abc import ABC, abstractmethod

from ompcbench.lib.application import *
from ompcbench.lib.io import Parser
from ompcbench.util import Counter
from ompcbench.check import verify_application_bin


class Application(ABC):
    """Groups data related to the application experimented on in a given case."""

    def __init__(self, all_env, cur_env, parser):
        # Application directory and binary to invoke
        self.cli = ""
        self.dir = parser.dir
        self.bin = cur_env.application.binary or all_env.application.binary
        self.flags = cur_env.application.flags or all_env.application.flags or []

        # Verify if application binary exists
        if parser.options.verify:
            verify_application_bin(self.dir, self.bin)

    @abstractmethod
    def __cmd__(self, case):
        """
        Build partial command to launch application in a particular case.
        Format is a list of arguments that must be appended to the launch command.
        Order of arguments is determined by relevant launcher scripts.
        """
        pass

    @abstractmethod
    def parse_file(fname, time_source="application"):
        """
        Extract information from job output file.
        Designed to be called directly, not from an object.
        "self" is undefined for this and the following functions.
        """
        pass

    @abstractmethod
    def get_dataframe_column_types():
        """
        Returns the type each column in the DataFrame should be converted to
        Columns defined here must be present in all executions must be removed
        """
        pass

    @abstractmethod
    def get_sort_values():
        """
        Returns sort values for the related DataFrame
        """
        pass

    @abstractmethod
    def get_output_values():
        """
        Returns list of output values of the application, excluding time
        """
        pass

    @abstractmethod
    def format_data(data, is_raw=False):
        """
        Improves DataFrame format, merging columns and optionally formatting values
        """
        pass

    @abstractmethod
    def get_pivot_index():
        """
        Return index to be used for the related DataFrame after a pivot
        """
        pass

    @abstractmethod
    def get_pivot_columns():
        """
        Returns columns to be maintained in the related DataFrame after a pivot
        List must not be empty
        """
        pass


def __parse_shell_time__(lines):
    """Parse elapsed time from `time` built-in."""
    return [
        float(line.strip().split()[5])
        for line in lines
        if line.startswith("==> Executed sample")
    ]


class Taskbench(Application):
    """Implements Application class for Taskbench applications."""

    def __cmd__(self, case):
        cmd = [
            f"{case.kernel}",
            f"{case.dep}",
            f"{case.size[0]}",  # width
            f"{case.size[1]}",  # steps
            f"{case.iter}",
            # TODO: Revisit this equation for output.
            f"{int(case.iter * 5 / case.CCR)}",
        ]

        return cmd

    # The filename pattern expected by OMPC Bench
    __regex__ = r"^(\w+)-N(\d+)-(\w+)-(\w+)-(\d+)i-([0-9\.]+)-(\d+)x(\d+)\.out"

    # Keep the regex above compile to prevent from recompiling every time
    # TODO: Check if it prevents recompiling even if inside a class
    __pattern__ = re.compile(__regex__)

    # Keys used to build a dictionary for each datapoint
    __metadata_keys__ = [
        "strategy",
        "nodes",
        "kernel",
        "dependency",
        "iterations",
        "CCR",
        "width",
        "steps",
    ]

    def parse_file(fname, time_source="taskbench"):
        self = Taskbench
        # Fill data dictionary with metadata
        bname = os.path.basename(fname)
        metadata = re.findall(self.__pattern__, bname)[0]
        metadata = dict(zip(self.__metadata_keys__, metadata))

        assert time_source in ("application", "taskbench", "shell")

        # Load data points from output file
        with open(fname, "r") as stream:
            lines = stream.readlines()

            def parse_taskbench_time(lines):
                """Parse elapsed time from TaskBench."""
                return [
                    float(line.strip().split()[2])
                    for line in lines
                    if line.startswith("Elapsed Time")
                ]

            times = (
                parse_taskbench_time(lines)
                if time_source == "taskbench" or time_source == "application"
                else __parse_shell_time__(lines)
            )
            jobid = next(
                int(line.split(":")[1])
                for line in lines
                if line.startswith("==> SLURM JOB ID")
            )

            metadata["jobid"] = jobid

        return list(
            # Return one record for each sample collected
            metadata | {"time": time, "sample": sample}
            for sample, time in enumerate(times)
        )

    def get_dataframe_column_types():
        dataframe_column_types = {
            "CCR": "float64",
            "dependency": "string",
            "iterations": "int64",
            "kernel": "string",
            "nodes": "int64",
            "steps": "string",
            "strategy": "string",
            "width": "string",
            "time": "float64",
        }

        return dataframe_column_types

    def get_sort_values():
        return ["dependency", "CCR", "iterations"]

    def get_output_values():
        return []

    def get_pivot_index():
        return ["taskgraph", "nodes", "kernel", "dependency", "strategy"]

    def get_pivot_columns():
        return ["CCR", "iterations"]

    def format_data(data, is_raw=False):
        # Represent taskgraph as `<width>x<steps>`
        data["taskgraph"] = data[["width", "steps"]].agg("x".join, axis=1)
        if not is_raw:
            # Represent iterations as millions
            data["iterations"] = data["iterations"].map(lambda x: x / 1e6)
        # Drop unused columns
        data = data.drop(["width", "steps"], axis=1)

        return data


class BlockMatMul(Application):
    """Implements Application class for Taskbench applications."""

    def __cmd__(self, case):
        cmd = [
            f"{case.size[0]}",  # matrix_size
            f"{case.size[1]}",  # block_size
            f"{case.matrix_size}",
            f"{case.block_size}",
        ]

        return cmd

    # The filename pattern expected by OMPC Bench
    __regex__ = r"^(\w+)-N(\d+)-(\d+)x(\d+)\.out"

    # Keep the regex above compile to prevent from recompiling every time
    # TODO: Check if it prevents recompiling even if inside a class
    __pattern__ = re.compile(__regex__)

    # Keys used to build a dictionary for each datapoint
    __metadata_keys__ = [
        "strategy",
        "nodes",
        "matrix_size",
        "block_size",
    ]

    def parse_file(fname, time_source="blockmatmul"):
        self = BlockMatMul
        # Fill data dictionary with metadata
        bname = os.path.basename(fname)
        metadata = re.findall(self.__pattern__, bname)[0]
        metadata = dict(zip(self.__metadata_keys__, metadata))

        assert time_source in ("application", "blockmatmul", "shell")

        # Load data points from output file
        with open(fname, "r") as stream:
            lines = stream.readlines()

            def parse_blockmatmul_time(lines):
                """Parse elapsed time from BlockMatMul."""
                return [
                    float(line.strip().split()[2][:-1])
                    for line in lines
                    if line.startswith("Elapsed Time")
                ]

            times = (
                parse_blockmatmul_time(lines)
                if time_source == "blockmatmul" or time_source == "application"
                else __parse_shell_time__(lines)
            )
            jobid = next(
                int(line.split(":")[1])
                for line in lines
                if line.startswith("==> SLURM JOB ID")
            )

            metadata["jobid"] = jobid

        return list(
            # Return one record for each sample collected
            metadata | {"time": time, "sample": sample}
            for sample, time in enumerate(times)
        )

    def get_dataframe_column_types():
        dataframe_column_types = {
            "block_size": "int64",
            "matrix_size": "int64",
            "nodes": "int64",
            "strategy": "string",
            "time": "float64",
        }

        return dataframe_column_types

    def get_sort_values():
        return ["strategy", "matrix_size", "block_size"]

    def get_output_values():
        return []

    def get_pivot_index():
        return ["nodes", "strategy"]

    def get_pivot_columns():
        return ["matrix_size", "block_size"]

    def format_data(data, is_raw=False):
        return data


class Plasma(Application):
    """Implements Application class for Plasma applications."""

    def __cmd__(self, case):
        cmd = [
            f"{case.routine}",
            f"{case.dim}",
            f"{case.nrhs}",
            f"{case.nb}",
            f"{case.nb_inner}",
            f"{case.test}",
            # TODO: Add case-based piping of output
        ]

        return cmd

    # The filename pattern expected by OMPC Bench
    __regex__ = (
        r"^(\w+)-N(\d+)-(\w+)-([0-9:]+)-([0-9:]+)-([0-9:]+)-([0-9:x]+)-([yn])\.out"
    )

    # Keep the regex above compile to prevent from recompiling every time
    # TODO: Check if it prevents recompiling even if inside a class
    __pattern__ = re.compile(__regex__)

    # Keys used to build a dictionary for each datapoint
    __metadata_keys__ = [
        "strategy",
        "nodes",
        "routine",
        "nrhs",
        "nb",
        "nb_inner",
        "dimensions",
        "test",
    ]

    def parse_file(fname, time_source="plasma"):
        self = Plasma
        # Fill data dictionary with metadata
        bname = os.path.basename(fname)
        metadata = re.findall(self.__pattern__, bname)[0]
        metadata = dict(zip(self.__metadata_keys__, metadata))
        for key in ["nrhs", "nb", "nb_inner", "dimensions", "test"]:
            del metadata[key]

        assert time_source in ("application", "plasma")

        # Load data points from output file
        with open(fname, "r") as stream:
            lines = stream.readlines()
            keys = next(
                line.strip().lower().split()
                for line in lines
                if line.strip().startswith("Status")
            )
            jobid = next(
                int(line.split(":")[1])
                for line in lines
                if line.startswith("==> SLURM JOB ID")
            )
            pattern = re.compile(r"\s+\+\s+")
            parameters = [
                re.sub(pattern, "+", line).strip().split()
                for line in lines
                if line.strip().startswith("--") or line.strip().startswith("pass")
            ]
        executions = [dict(zip(keys, e)) for e in parameters]

        def iterable_counter(executions):
            counter = Counter()
            for e in executions:
                # Count concatenations of all values but the first four:
                # (Status, Error, Time and Gflop/s)
                l = " ".join((list(e.values()))[4::])
                yield counter.get(l), e

        return list(
            # Return one record for each sample collected
            metadata | execution | {"jobid": jobid, "sample": sample}
            for sample, execution in iterable_counter(executions)
        )

    def get_dataframe_column_types():
        dataframe_column_types = {
            "gflop/s": "float64",
            "status": "string",
            "time": "float64",
            "n": "int64",
            "nb": "int64",
            "nb_i": "int64",
            "nodes": "int64",
            "pada": "int64",
            "routine": "string",
            "strategy": "string",
        }

        return dataframe_column_types

    def get_sort_values():
        return ["routine", "n", "nb", "nb_i"]

    def get_output_values():
        return ["gflop/s"]

    def get_pivot_index():
        return ["n", "nb", "nb_i", "nodes", "strategy"]

    def get_pivot_columns():
        return ["routine"]

    def format_data(data, is_raw=False):
        return data


__apps__ = {"taskbench": Taskbench, "plasma": Plasma, "blockmatmul": BlockMatMul}


def get_app(all_env, cur_env, parser):
    """Return Application object constructed from arguments."""
    return __apps__[parser.type](all_env, cur_env, parser)


def get_uninitialized_app(name):
    """Return Application object that has not been initialized."""
    return __apps__[name]
