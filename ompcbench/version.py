from ompcbench import __version__


def version_compatible(version):
    """Check if a version string is compatible the tool's version"""
    tool_version = tuple(__version__.split("."))[:2]
    config_version = tuple(version.split("."))
    parts_match = [t == c for t, c in zip(tool_version, config_version)]
    return all(parts_match)
