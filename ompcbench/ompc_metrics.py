import orjson
import pandas as pd

from ompcbench.util import (
    get_dir_list,
    get_lower_ts,
    get_finalization_time,
    get_assoc_end_evt,
)

from numpy import average
from pathlib import Path

# Global event name lists
# ==============================================================================

# Events names and lists
from ompcbench.util import COMM_EVTS, TASK_EVT, NUM_DECIMALS

# Events that are outside the parallel region
NON_PARALLEL_EVTS = [
    "Event System Execution",
    "Gate Thread Execution",
    "Event Handler Execution",
    "Event System / Construction",
    "Event System / Destruction",
    "Exit / Wait",
    "Exit / Begin",
    "Exit / End",
    "PinTask",
]

# Initialization functions
# ==============================================================================


def initialize_time_dict(times_by_pid_tid, pid, tid):
    """
    Returns the time dictionary with value 0 in computation, communication, and
    parallel times.
    """
    times_by_pid_tid[pid][tid] = {
        "comp_time": 0,
        "com_time": 0,
        "parallel_time": 0,
    }


def initialize_evts_dict(evts_by_pid_tid, pid, tid):
    """
    Returns the events dictionary with empty lists of a given pid/tid in
    execute, parallel, begin of communication, and end of communication.
    """
    evts_by_pid_tid[pid][tid] = {
        "execute_evts": [],
        "parallel_evts": [],
        "begin_com_evts": [],
        "end_com_evts": [],
    }


# Communication metrics functions
# ==============================================================================


def get_com_evts_pair(evts, pid, tid, end_com_evts):
    """Return the associated Begin / End communication events pairs."""
    for begin_com_evt in evts[pid][tid]["begin_com_evts"]:
        # Search associated end event in the same thread
        end_com_evt = get_assoc_end_evt(begin_com_evt, evts[pid][tid]["end_com_evts"])

        if end_com_evt is None:
            # If the associated end event is not on the same thread
            # Search associated end event in all threads
            end_com_evt = get_assoc_end_evt(begin_com_evt, end_com_evts)

            # Remove the end event to not be associated with another begin event
            end_com_evts.remove(end_com_evt)
        else:
            evts[pid][tid]["end_com_evts"].remove(end_com_evt)

        # Add communication pair
        if "com_evts_pairs" not in evts[pid][tid]:
            evts[pid][tid]["com_evts_pairs"] = []
        evts[pid][tid]["com_evts_pairs"].append((begin_com_evt, end_com_evt))


def get_pair_time_points(pair):
    """Return a tuple with begin and end time points on the timeline."""
    begin_evt, end_evt = pair
    return (begin_evt["ts"], end_evt["ts"] + end_evt["dur"])


def get_com_time(com_evts_pairs):
    """
    Returns the accumulated communication time from a communication pair list.
    """
    com_time = 0

    # Sort communication pairs by begin time
    com_evts_pairs = sorted(com_evts_pairs, key=lambda com_pair: com_pair[0]["ts"])

    # Merge interleaved communications pairs
    while len(com_evts_pairs) > 0:
        # Get communication pair with lower begin time
        main_com_pair = com_evts_pairs.pop(0)
        main_begin_time, main_end_time = get_pair_time_points(main_com_pair)

        # Search for interleaved pairs
        removed_pairs = []
        for com_pair in com_evts_pairs:
            begin_evt, end_evt = com_pair

            # The pair is interleaved if its begin time is placed between the
            # begin time and end time of the main communication pair
            if begin_evt["ts"] > main_begin_time and begin_evt["ts"] < main_end_time:
                # Merge time points
                begin_time, end_time = get_pair_time_points(com_pair)
                main_begin_time = min([main_begin_time, begin_time])
                main_end_time = max([main_end_time, end_time])

                # Add merged communication pair to be removed
                removed_pairs.append(com_pair)
            else:
                # As the list is sorted, if the current pair is not interleaved,
                # the others are not interleaved either
                break

        # Remove communication pairs that have already been counted
        for removed_pair in removed_pairs:
            com_evts_pairs.remove(removed_pair)

        # Accumulate communication time
        com_time += main_end_time - main_begin_time

    return com_time


def get_communication_metrics_lists(
    evts_by_pid_tid, times_by_pid_tid, pid, tid, end_com_evts, com_times, com_efcs
):
    """Get lists of communication times and efficiencies."""
    # Separate communication pairs (Begin / End) to calculate communication time
    get_com_evts_pair(evts_by_pid_tid, pid, tid, end_com_evts)

    # Calculate the communication time of a single Data Event thread
    times_by_pid_tid[pid][tid]["com_time"] = get_com_time(
        evts_by_pid_tid[pid][tid]["com_evts_pairs"]
    )

    # Calculate the communication efficiency of a single Data Event thread
    if times_by_pid_tid[pid][tid]["com_time"] != 0:
        com_times.append(times_by_pid_tid[pid][tid]["com_time"])
        com_efcs.append(
            times_by_pid_tid[pid][tid]["com_time"]
            / times_by_pid_tid[pid][tid]["parallel_time"]
        )


# Computation metrics functions
# ==============================================================================


def get_computation_metrics_lists(times_by_pid_tid, pid, tid, comp_times, comp_efcs):
    """Get lists of computation times and efficiencies."""
    # Calculate the computation time of a single Execute Event Handler
    comp_times.append(times_by_pid_tid[pid][tid]["comp_time"])

    # Calculate the computation efficiency of a single Execute Event Handler
    comp_efcs.append(
        times_by_pid_tid[pid][tid]["comp_time"]
        / (times_by_pid_tid[pid][tid]["parallel_time"])
    )


# Parallel metrics functions
# ==============================================================================


def get_parallel_metrics_lists(
    times_by_pid_tid, pid, tid, parallel_times, thread_parallel_efcs
):
    """Get lists of parallel times and thread parallel efficiencies."""
    # List of parallel time
    parallel_times.append(times_by_pid_tid[pid][tid]["parallel_time"])

    # Calculate thread parallel efficiency of a single Execute Event Handler
    thread_parallel = (
        times_by_pid_tid[pid][tid]["parallel_time"]
        - times_by_pid_tid[pid][tid]["comp_time"]
    ) / times_by_pid_tid[pid][tid]["parallel_time"]

    # List of thread parallel efficiencies
    thread_parallel_efcs.append(thread_parallel)


def get_parallel_time(parallel_evts):
    """Returns difference between begin and end parallel time."""
    # Get begin parallel time
    lower_ts_parallel_evts = get_lower_ts(parallel_evts)
    # Get end parallel time
    finalization_time_parallel_evts = get_finalization_time(parallel_evts)

    return finalization_time_parallel_evts - lower_ts_parallel_evts


# Formatting functions
# ==============================================================================


def get_formatted_time(time_us):
    """
    Return the time in hours:minutes:seconds:milliseconds format, given the time
    in microseconds.
    """
    # Time unit components
    time_h = 0
    time_min = 0
    time_sec = 0

    # Time constants
    HOUR_DIV = 3600000
    MIN_DIV = 60000
    SEC_MSEC_DIV = 1000

    # Convert microseconds to milliseconds
    time_ms = time_us / SEC_MSEC_DIV

    # Get time component of hours
    if time_ms / HOUR_DIV > 0:
        time_h = (int)(time_ms / HOUR_DIV)
        time_ms -= time_h * HOUR_DIV
    # Get time component of minutes
    if time_ms / MIN_DIV > 0:
        time_min = (int)(time_ms / MIN_DIV)
        time_ms -= time_min * MIN_DIV
    # Get time component of seconds
    if time_ms / SEC_MSEC_DIV > 0:
        time_sec = (int)(time_ms / SEC_MSEC_DIV)
        time_ms -= time_sec * SEC_MSEC_DIV
    # Round millisecond component
    time_ms = round(time_ms)

    return f"{time_h}:{time_min}:{time_sec}:{time_ms}"


def get_formated_metric(metric_value, num_decimals=NUM_DECIMALS):
    """
    Returns a metric value rounded to a defined number of decimals (default or
    given by parameter).
    """
    return round(metric_value, num_decimals)


# Metric calculation functions
# ==============================================================================


def calculate_load_balance(accumulated_time_list):
    """
    Returns the Load Balance formatted metric for a given accumulated time list.
    """
    return get_formated_metric(
        average(accumulated_time_list) / max(accumulated_time_list)
    )


def calculate_efficiency(metric_efcs):
    """
    Returns the Efficiency formatted metric for a given metric efficiency list.
    """
    return get_formated_metric(max(metric_efcs))


def calculate_parallel_efc(load_balance, metric_efc):
    """
    Returns the Parallel Efficiency formatted metric for a given load balance
    efficiency and metric efficiency.
    """
    return get_formated_metric(load_balance * metric_efc)


def calculate_serial_time(parallel_evts_head_process, total_time):
    """
    Returns the serial time calculated by subtracting the parallel time from the
    total time of the head process.
    """
    parallel_time_head_process = get_finalization_time(
        parallel_evts_head_process
    ) - get_lower_ts(parallel_evts_head_process)
    return total_time - parallel_time_head_process


def calculate_serial_region_efc(serial_time, total_time):
    """
    Returns the Serial Region Efficiency formatted metric for a given serial
    time and total time.
    """
    return get_formated_metric(serial_time / total_time)


def calculate_process_load_balance(parallel_times, total_time):
    """
    Returns the Process Load Balance formatted metric for a given total time and
    a list of parallel times.
    """
    return get_formated_metric(
        1 - ((max(parallel_times) - average(parallel_times)) / total_time)
    )


def calculate_process_com_efc(parallel_times, serial_time, total_time):
    """
    Returns the Process Communication Efficiency formatted metric for a given
    serial time, total time and a list of parallel times.
    """
    return get_formated_metric((max(parallel_times) + serial_time) / total_time)


def calculate_process_efc(process_load_balance_efc, process_com_efc):
    """
    Returns the Process Efficiency formatted metric for a given process load
    balance and process communication efficiency.
    """
    return get_formated_metric(process_load_balance_efc * process_com_efc)


def calculate_comp_time_percentage(comp_times, total_parallel_time):
    """
    Returns the Computation Time formatted metric in a accumulated percentage of
    parallel time.
    """
    return get_formated_metric(
        (sum(comp_times) * 100) / total_parallel_time, num_decimals=1
    )


# Metric extraction function
# ==============================================================================


def extract_ompc_metrics(output, root_dir=None, trace_files=None):
    """
    Extract OMPC metrics from multiple OMPC executions divided into separate
    folders.
    """

    # Get list of directories to extract metrics separately
    if trace_files is None:
        # Each folder has a different OMPCluster execution
        dir_list = get_dir_list(root_dir)
    else:
        # Only one folder
        dir_list = [root_dir]

    # Pandas DataFrame with all general metrics
    pd.set_option("display.precision", NUM_DECIMALS)
    all_general_metrics_df = pd.DataFrame({})

    for directory in dir_list:
        pid_head_process = -1

        # Events related to metrics separated by pid and tid
        evts_by_pid_tid = {}

        # Events lists
        end_com_evts = []

        # Parallel and computation time of each thread
        times_by_pid_tid = {}

        # Metrics vars
        num_threads = 0
        num_tasks = 0
        total_parallel_time = 0

        # Metrics lists
        comp_efcs = []
        comp_times = []
        com_efcs = []
        com_times = []
        finalization_times = []
        parallel_times = []
        thread_parallel_efcs = []

        # Get trace files when there are multiple OMPC executions
        if trace_files is None:
            trace_files = directory.glob("*.json")

        for tracing_file in trace_files:
            tracing = open(tracing_file, "r").read()

            # Convert trace to JSON objects
            objs = orjson.loads(tracing)

            # Get lower ts to synchronize events
            lower_ts = get_lower_ts(objs["traceEvents"])

            for evt in objs["traceEvents"]:
                if evt["ph"] == "X":
                    # Synchronize evts
                    evt["ts"] = evt["ts"] - lower_ts

                    pid = evt["pid"]
                    tid = evt["tid"]

                    # Initialize time and event dicts
                    if pid not in evts_by_pid_tid:
                        evts_by_pid_tid[pid] = {}
                        times_by_pid_tid[pid] = {}
                    if tid not in evts_by_pid_tid[pid]:
                        initialize_time_dict(times_by_pid_tid, pid, tid)
                        initialize_evts_dict(evts_by_pid_tid, pid, tid)

                    # Separate events by pid / tid
                    if evt["name"] == TASK_EVT:
                        evts_by_pid_tid[pid][tid]["execute_evts"].append(evt)
                        evts_by_pid_tid[pid][tid]["parallel_evts"].append(evt)
                        # Accumulate computation time
                        times_by_pid_tid[pid][tid]["comp_time"] += evt["dur"]
                        # Accumulate number of tasks
                        num_tasks += 1
                    elif evt["name"] in COMM_EVTS:
                        evts_by_pid_tid[pid][tid]["parallel_evts"].append(evt)
                        # Separate Begin and End communication events
                        if "Begin" in evt["name"]:
                            evts_by_pid_tid[pid][tid]["begin_com_evts"].append(evt)
                        else:
                            evts_by_pid_tid[pid][tid]["end_com_evts"].append(evt)
                            end_com_evts.append(evt)
                    elif (
                        evt["name"] not in NON_PARALLEL_EVTS
                        and "__tgt_rtl" not in evt["name"]
                    ):
                        evts_by_pid_tid[pid][tid]["parallel_evts"].append(evt)
                # Get pid head process
                if evt["ph"] == "M" and evt["name"] == "process_name":
                    if evt["args"]["name"] == "Head Process":
                        pid_head_process = pid

            # Get the longest finalization time for each trace file to calculate
            # the total time
            finalization_times.append(get_finalization_time(objs["traceEvents"]))

        # Get total time
        total_time = max(finalization_times)

        # Get lists of metrics and accumulate threads and parallel time
        parallel_evts_head_process = []
        for pid in evts_by_pid_tid.keys():
            for tid in evts_by_pid_tid[pid].keys():
                # Get number of threads
                # Only threads with parallel events are counted
                num_threads += 1

                # Get parallel time
                parallel_evts = evts_by_pid_tid[pid][tid]["parallel_evts"]
                if parallel_evts:
                    times_by_pid_tid[pid][tid]["parallel_time"] = get_parallel_time(
                        parallel_evts
                    )

                if pid == pid_head_process:
                    # Get parallel events of head process to calculate serial
                    # time
                    parallel_evts_head_process.extend(parallel_evts)
                else:
                    # Current thread is a Data Thread
                    if len(evts_by_pid_tid[pid][tid]["begin_com_evts"]) > 0:
                        # Get communication lists of time and efficiency
                        get_communication_metrics_lists(
                            evts_by_pid_tid,
                            times_by_pid_tid,
                            pid,
                            tid,
                            end_com_evts,
                            com_times,
                            com_efcs,
                        )

                    # Current thread is a Execute Thread
                    if times_by_pid_tid[pid][tid]["comp_time"] != 0:
                        # Get computation lists of time and efficiency
                        get_computation_metrics_lists(
                            times_by_pid_tid, pid, tid, comp_times, comp_efcs
                        )

                        # Get the list of parallel metrics and accumulate
                        # parallel time
                        if parallel_evts:
                            total_parallel_time += times_by_pid_tid[pid][tid][
                                "parallel_time"
                            ]
                            get_parallel_metrics_lists(
                                times_by_pid_tid,
                                pid,
                                tid,
                                parallel_times,
                                thread_parallel_efcs,
                            )

        # Calculate computation metrics
        comp_load_balance = calculate_load_balance(comp_times)
        comp_efc = calculate_efficiency(comp_efcs)
        comp_parallel_efc = calculate_parallel_efc(comp_load_balance, comp_efc)

        # Calculate communication metrics
        com_load_balance = calculate_load_balance(com_times)
        com_efc = calculate_efficiency(com_efcs)
        com_parallel_efc = calculate_parallel_efc(com_load_balance, com_efc)

        # Calculate serial efficiency
        serial_time = calculate_serial_time(parallel_evts_head_process, total_time)
        serial_region_efc = calculate_serial_region_efc(serial_time, total_time)

        # Calculate process metrics
        process_load_balance_efc = calculate_process_load_balance(
            parallel_times, total_time
        )
        process_com_efc = calculate_process_com_efc(
            parallel_times, serial_time, total_time
        )
        process_efc = calculate_process_efc(process_load_balance_efc, process_com_efc)

        # Calculate computation time
        comp_time_percentage = calculate_comp_time_percentage(
            comp_times, total_parallel_time
        )

        # All general metrics of a single execution
        general_metrics = {
            "Name": [directory.name],
            "Worker Process": [len(evts_by_pid_tid) - 1],
            "Threads": [num_threads],
            "Number of Tasks": [num_tasks],
            ("Total time [h:min:sec:ms]"): [get_formatted_time(total_time)],
            ("Parallel Time [h:min:sec:ms]"): [get_formatted_time(total_parallel_time)],
            ("Computation Time [%]"): [comp_time_percentage],
            "Computation Load Balance Efficiency": [comp_load_balance],
            "Computation Efficiency": [comp_efc],
            "Computation Parallel Efficiency": [comp_parallel_efc],
            "Communication Load Balance Efficiency": [com_load_balance],
            "Communication Efficiency": [com_efc],
            "Communication Parallel Efficiency": [com_parallel_efc],
            "Process Load Balance Efficiency": [process_load_balance_efc],
            "Process Communication Efficiency": [process_com_efc],
            "Process Efficiency": [process_efc],
            "Serial Region Efficiency": [serial_region_efc],
        }

        general_metrics_df = pd.DataFrame(general_metrics)

        # Concatenate metrics from other executions
        if all_general_metrics_df.empty:
            all_general_metrics_df = general_metrics_df
        else:
            all_general_metrics_df = pd.concat(
                [all_general_metrics_df, general_metrics_df], axis=0
            )

    # Sort metrics by application name
    all_general_metrics_df = all_general_metrics_df.sort_values(by=["Name"])

    # Write results
    all_general_metrics_df.to_csv(output, index=False)
