from ompcbench.ompc_metrics import extract_ompc_metrics
from ompcbench.task_metrics import extract_task_metrics
from ompcbench.critical_path_metrics import extract_critical_path_metrics


def extract_metrics_by_option(
    output, root_dir=None, trace_files=None, metric_op="ompc"
):
    """
    Extract chosen metrics: OMPC or task.
    """
    if metric_op == "ompc":
        extract_ompc_metrics(output, root_dir, trace_files)
    elif metric_op == "task":
        extract_task_metrics(output, root_dir, trace_files)
    elif metric_op == "critical-path":
        extract_critical_path_metrics(output, root_dir, trace_files)
