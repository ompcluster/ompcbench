import orjson
import pandas as pd

from ompcbench.util import (
    get_dir_list,
    get_output_prefix_suffix,
    get_inner_evts,
    get_mpi_tag,
    get_nearest_evt,
)

from pathlib import Path

# Global events names
# ==============================================================================

from ompcbench.util import (
    TARGET_EVTS,
    TARGET_BEGIN_EVTS,
    TARGET_END_EVTS,
    TASK_EVT,
    EXCHANGE_BEGIN_EVT,
    EXCHANGE_END_EVT,
    SUBMIT_BEGIN_EVT,
    SUBMIT_END_EVT,
    RETRIEVE_BEGIN_EVT,
    RETRIEVE_END_EVT,
    PUSH_NEW_EVT,
    NUM_DECIMALS,
)

EXEC = "Execute"
EXCH = "Exchange"
SUB = "Submit"
RETR = "Retrieve"
EXEC_TASK = "Execute Task"

# Metric calculation functions
# ==============================================================================


def get_metric_calculation_df(metric_df, metric_name, dur_time, percentage=True):
    """
    Return a dataframe with two columns: metric time duration and its percentage
    over total time.
    """
    index_labels = [metric_name]
    metric_result_df = round(
        pd.DataFrame(metric_df, columns=index_labels), NUM_DECIMALS
    )
    if percentage:
        metric_result_df[metric_name + " %"] = round(
            metric_result_df[metric_name] * (100 / dur_time), NUM_DECIMALS
        )
    return metric_result_df


def get_metrics_concat(durations, type_name, dur_time):
    """Returns all task metrics concatenated in a single dataframe."""
    # Create transposed dataframe from durations dict
    all_metrics_df = pd.DataFrame.from_dict(durations, orient="index").T

    # Calculate task metrics
    average_df = get_metric_calculation_df(all_metrics_df.mean(), "AVG", dur_time)
    min_df = get_metric_calculation_df(all_metrics_df.min(), "MIN", dur_time)
    max_df = get_metric_calculation_df(all_metrics_df.max(), "MAX", dur_time)
    count_df = get_metric_calculation_df(
        all_metrics_df.count(), "COUNT", dur_time, False
    )
    sum_df = get_metric_calculation_df(all_metrics_df.sum(), "SUM", dur_time)

    # Concatenate all task metrics
    all_metrics = pd.concat([average_df, max_df], axis=1)
    all_metrics = pd.concat([all_metrics, min_df], axis=1)
    all_metrics = pd.concat([all_metrics, count_df], axis=1)
    all_metrics = pd.concat([all_metrics, sum_df], axis=1)
    all_metrics["Type"] = type_name

    return all_metrics


def calculate_comm_durations(evt_type, comm_pairs, source_locations, durations):
    """
    Calculates communication durations using communication pairs and saves the
    results separated by source location in a dictionary.
    """
    for evt_pair in comm_pairs[evt_type]:
        begin, end = evt_pair
        if get_mpi_tag(begin) in source_locations[evt_type]:
            source_location = source_locations[evt_type][get_mpi_tag(begin)]
            if source_location not in durations[evt_type]:
                durations[evt_type][source_location] = []
            durations[evt_type][source_location].append(
                end["ts"] + end["dur"] - begin["ts"]
            )


def get_comm_pairs(begin_evts, end_evts):
    """Returns a list of tuples with the communication pairs."""
    comm_pairs = []
    for begin_evt in begin_evts:
        # For each event of begin find all possible matching end events
        # A matching end event must have the same mpi tag and start after the
        # begin event.
        match_end_evts = [
            end_evt
            for end_evt in end_evts
            if get_mpi_tag(end_evt) == get_mpi_tag(begin_evt)
            and end_evt["ts"] > begin_evt["ts"]
        ]
        # If there is more than one matching end event, get the nearest one
        end_evt = get_nearest_evt(begin_evt, match_end_evts)
        comm_pairs.append((begin_evt, end_evt))
    return comm_pairs


def update_source_location(evt_type, target_evt, push_evts, source_locations):
    """
    Adds the source locations in a dictionary separated by the MPI tag and the
    event type.
    """
    inner_push_evts = get_inner_evts(target_evt, push_evts[evt_type])
    if inner_push_evts is not None:
        for push_evt in inner_push_evts:
            source_locations[evt_type][get_mpi_tag(push_evt)] = target_evt["args"][
                "source_location"
            ]


def get_evts_acc_time(evts, comm_evts=False):
    """Returns the sum of the duration of all given events."""
    acc_time = 0
    if comm_evts:
        for event_type in evts.keys():
            if event_type != EXEC:
                for source_loc in evts[event_type].keys():
                    for dur in evts[event_type][source_loc]:
                        acc_time += dur
    else:
        for evt in evts:
            acc_time += evt["dur"]
    return acc_time


def get_task_metrics(trace_evts):
    """
    Returns a dataframe with all task metrics extracted from a single OMPC
    execution.
    """
    # Initialize events lists and dictionaries
    # ==========================================================================

    # Target Events
    target_evts = []
    target_begin_evts = []
    target_end_evts = []

    # Task Events
    task_evts = []

    # Communication Events Separation
    comm_evts = {
        EXCHANGE_BEGIN_EVT: [],
        EXCHANGE_END_EVT: [],
        SUBMIT_BEGIN_EVT: [],
        SUBMIT_END_EVT: [],
        RETRIEVE_BEGIN_EVT: [],
        RETRIEVE_END_EVT: [],
    }

    # Push Events Separation
    push_evts = {EXEC: [], EXCH: [], SUB: [], RETR: []}

    # Source Locations Separation
    source_locations = {EXEC: {}, EXCH: {}, SUB: {}, RETR: {}}

    # Communication Pairs Separation
    comm_pairs = {EXCH: [], SUB: [], RETR: []}

    # Durations Separation
    durations = {EXEC: {}, EXCH: {}, SUB: {}, RETR: {}}

    # All metrics DataFrame
    all_task_metrics_df = pd.DataFrame({})

    # Get pids of the worker threads
    worker_thread_pids = []
    for evt in trace_evts:
        if evt["ph"] == "M" and evt["name"] == "thread_name":
            if evt["args"]["name"] == "Worker Thread":
                worker_thread_pids.append(evt["pid"])

    # Separate events by type
    for evt in trace_evts:
        if evt["pid"] in worker_thread_pids:
            if evt["name"] in TARGET_EVTS:
                target_evts.append(evt)
            elif evt["name"] in TARGET_BEGIN_EVTS:
                target_begin_evts.append(evt)
            elif evt["name"] in TARGET_END_EVTS:
                target_end_evts.append(evt)
            elif evt["name"] == PUSH_NEW_EVT:
                if evt["args"]["event_type"] == EXEC:
                    push_evts[EXEC].append(evt)
                elif evt["args"]["event_type"] == EXCH:
                    push_evts[EXCH].append(evt)
                elif evt["args"]["event_type"] == SUB:
                    push_evts[SUB].append(evt)
                elif evt["args"]["event_type"] == RETR:
                    push_evts[RETR].append(evt)
        else:
            if evt["name"] == TASK_EVT:
                task_evts.append(evt)
            elif evt["name"] == EXCHANGE_BEGIN_EVT:
                comm_evts[EXCHANGE_BEGIN_EVT].append(evt)
            elif evt["name"] == EXCHANGE_END_EVT:
                comm_evts[EXCHANGE_END_EVT].append(evt)
            elif evt["name"] == SUBMIT_BEGIN_EVT:
                comm_evts[SUBMIT_BEGIN_EVT].append(evt)
            elif evt["name"] == SUBMIT_END_EVT:
                comm_evts[SUBMIT_END_EVT].append(evt)
            elif evt["name"] == RETRIEVE_BEGIN_EVT:
                comm_evts[RETRIEVE_BEGIN_EVT].append(evt)
            elif evt["name"] == RETRIEVE_END_EVT:
                comm_evts[RETRIEVE_END_EVT].append(evt)

    # Update source location for all target events
    for target_evt in target_evts:
        update_source_location(EXEC, target_evt, push_evts, source_locations)
        update_source_location(EXCH, target_evt, push_evts, source_locations)
        update_source_location(SUB, target_evt, push_evts, source_locations)
        update_source_location(RETR, target_evt, push_evts, source_locations)
    for target_begin_evt in target_begin_evts:
        update_source_location(SUB, target_begin_evt, push_evts, source_locations)
    for target_end_evt in target_end_evts:
        update_source_location(RETR, target_end_evt, push_evts, source_locations)

    # Separate all task durations by source location
    for task_evt in task_evts:
        source_location = source_locations[EXEC][get_mpi_tag(task_evt)]
        if source_location not in durations[EXEC]:
            durations[EXEC][source_location] = []
        durations[EXEC][source_location].append(task_evt["dur"])

    # Get all communication pairs to calculate the duration
    comm_pairs[EXCH] = get_comm_pairs(
        comm_evts[EXCHANGE_BEGIN_EVT], comm_evts[EXCHANGE_END_EVT]
    )
    comm_pairs[SUB] = get_comm_pairs(
        comm_evts[SUBMIT_BEGIN_EVT], comm_evts[SUBMIT_END_EVT]
    )
    comm_pairs[RETR] = get_comm_pairs(
        comm_evts[RETRIEVE_BEGIN_EVT], comm_evts[RETRIEVE_END_EVT]
    )

    # Separate all communication durations by source location
    calculate_comm_durations(EXCH, comm_pairs, source_locations, durations)
    calculate_comm_durations(SUB, comm_pairs, source_locations, durations)
    calculate_comm_durations(RETR, comm_pairs, source_locations, durations)

    # Get communication and task events accumulated time
    acc_time_data = get_evts_acc_time(durations, comm_evts=True)
    acc_time_exec = get_evts_acc_time(task_evts)

    # Calculate task metrics
    if durations[EXEC]:
        metrics_df = get_metrics_concat(durations[EXEC], EXEC_TASK, acc_time_exec)
        all_task_metrics_df = pd.concat([metrics_df, all_task_metrics_df])
    if durations[EXCH]:
        metrics_df = get_metrics_concat(durations[EXCH], EXCH, acc_time_data)
        all_task_metrics_df = pd.concat([metrics_df, all_task_metrics_df])
    if durations[SUB]:
        metrics_df = get_metrics_concat(durations[SUB], SUB, acc_time_data)
        all_task_metrics_df = pd.concat([metrics_df, all_task_metrics_df])
    if durations[RETR]:
        metrics_df = get_metrics_concat(durations[RETR], RETR, acc_time_data)
        all_task_metrics_df = pd.concat([metrics_df, all_task_metrics_df])

    return all_task_metrics_df


# Metric extraction function
# ==============================================================================


def extract_task_metrics(output, root_dir=None, trace_files=None):
    """
    Extract task metrics from multiple OMPC executions divided into separate
    folders.
    """

    # Get list of directories to extract metrics separately
    if trace_files is None:
        # Each folder has a different OMPCluster execution
        dir_list = get_dir_list(root_dir)
    else:
        # Only one folder
        dir_list = [root_dir]

    # Separate the output suffix and prefix
    output_prefix, output_suffix = get_output_prefix_suffix(output)

    suffix_count = 0
    for directory in dir_list:
        # Get trace files when there are multiple OMPC executions
        if trace_files is None:
            trace_files = directory.glob("*.json")

        # Get all events from a single OMPC execution
        all_evts = []
        for tracing_file in trace_files:
            tracing = open(tracing_file, "r").read()
            # Convert trace to JSON objects
            objs = orjson.loads(tracing)
            all_evts.extend(objs["traceEvents"])

        # Get task metrics
        metrics = get_task_metrics(all_evts)

        # Write task metrics from different OMPC executions in separate files
        metrics.to_csv(output_prefix + str(suffix_count) + output_suffix, index=True)
        suffix_count += 1
