import json
import pygraphviz as pgv

from pathlib import Path
from ompcbench.util import (
    is_inner_event,
    get_lower_ts,
    get_nearest_evt,
    get_evts_same_mpi_tag,
    get_mpi_tag,
    get_name_prefix,
    create_id,
    create_edge_evt,
    BEGIN_EVT,
    END_EVT,
    TIME_AS_BEGIN,
    TIME_AS_END,
    COMM_EVTS,
    PUSH_COMM_EVTS,
    TARGET_EVTS,
    TASK_EVT,
    SCHEDULE_EVT,
    PUSH_NEW_EVT,
)

# Critical Path Constants
# ==============================================================================

# Color of critical path edges
CRITICAL_PATH_COLOR = "green"

# Indicates if a graph node is root or not
ROOT = 1
NOT_ROOT = 0


def get_task_node_name(task_id):
    """Return the graph node name related to a task id."""
    return "V" + str(task_id)


def calculate_critical_path(root_node, paths_time, task_evts, graph):
    """Calculate the critical path from a root node of a task graph."""
    longest_time = 0
    edge_longest_time = None

    # Iterates through root node in-out dependencies
    for edge in graph.edges_iter(root_node):
        (in_node, out_node) = edge
        # Only out dependencies are required
        if in_node == root_node:
            # Check if critical path of this node has been calculated yet
            if out_node not in paths_time.keys():
                # Calculate the critical path of out dependence
                calculate_critical_path(out_node, paths_time, task_evts, graph)

            # Check if the critical path of out dependence is longer than other
            # out dependencies
            out_time = paths_time[out_node] + (
                task_evts[out_node]["ts"]
                - (task_evts[root_node]["ts"] + task_evts[root_node]["dur"])
            )
            if out_time > longest_time:
                longest_time = out_time
                edge_longest_time = edge

    # Color the edge of green to mark the critical path
    if edge_longest_time is not None:
        edge_longest_time.attr["color"] = CRITICAL_PATH_COLOR

    # Update the critical path of the root node
    paths_time[root_node] = task_evts[root_node]["dur"] + longest_time


def get_critical_path(root_node, graph, edges_critical_path):
    """Return the critical path edges of a root node in a task graph."""
    for edge in graph.edges_iter(root_node):
        (in_node, out_node) = edge
        # Check if an edge is in the critical path
        if in_node == root_node and edge.attr["color"] == CRITICAL_PATH_COLOR:
            edges_critical_path.append(edge)
            # Get the critical path of out dependence node
            get_critical_path(out_node, graph, edges_critical_path)


def get_evts_by_task_id(task_id, evts):
    """Returns a list of events with the same id as the task graph."""
    return [e for e in evts if get_task_node_name(e["args"]["task_id"]) == task_id]


def get_evts_by_type(evt_type, evts, evt_name=""):
    """
    Returns a list of events of type Begin or End. It is possible to pass the
    event name to return specific Begin or End events.
    """
    evt_type_str = "Begin"
    if evt_type == END_EVT:
        evt_type_str = "End"
    if evt_name == "":
        return [e for e in evts if evt_type_str in e["name"]]
    else:
        return [e for e in evts if evt_type_str in e["name"] and evt_name in e["name"]]


def get_schedule_evt(main_evt, schedule_evts):
    """Returns the schedule event innered to a main event."""
    inner_schedule_evts = [e for e in schedule_evts if is_inner_event(main_evt, e)]
    schedule_evt = get_nearest_evt(main_evt, inner_schedule_evts)
    return schedule_evt


def get_push_evts(main_evt, push_evts, push_evt_type_list):
    """Returns specific types push events inner to the main event."""
    inner_push_evts = [e for e in push_evts if is_inner_event(main_evt, e)]

    # Get executed event related to a push event
    push_type_evts = [
        e for e in inner_push_evts if e["args"]["event_type"] in push_evt_type_list
    ]

    return push_type_evts


def get_worker_com_evts(push_com_evts, com_evts, schedule_evt, pid_head_process):
    """Returns communication events from worker nodes related to push events."""
    worker_com_evts_all = []
    for push_evt in push_com_evts:
        # Get worker nodes communication events related to a push event
        com_evts_same_tag = get_evts_same_mpi_tag(push_evt, com_evts)
        worker_com_evts = list(
            filter(
                lambda com_evt: com_evt["pid"] != pid_head_process
                and push_evt["args"]["event_type"] in com_evt["name"],
                com_evts_same_tag,
            )
        )
        # Add task id to communication events
        for evt in worker_com_evts:
            evt["args"]["task_id"] = schedule_evt["args"]["task_id"]
        worker_com_evts_all.extend(worker_com_evts)

    return worker_com_evts_all


def get_task_evts_by_id(execute_evts, task_ids, pid_head_process):
    """Returns a dictionary that relates task ids to task events."""
    task_evts = {}
    for execute in execute_evts:
        mpi_tag = get_mpi_tag(execute)
        if mpi_tag in task_ids:
            # Add task id to execute target event
            execute["args"]["task_id"] = task_ids[mpi_tag]
            # Stores task time and execute target event
            if execute["pid"] != pid_head_process:
                task_id = task_ids[mpi_tag]
                task_evts[get_task_node_name(task_id)] = execute
    return task_evts


def get_critical_path_evts(graph_file, task_evts, dir_name, pid_head_process):
    """Returns tasks and edge events that belong to the critical path."""
    graph = pgv.AGraph(graph_file, strict=False, directed=True)
    # Ensure the color of the edges is different from CRITICAL_PATH_COLOR
    graph.edge_attr.update(color="black")

    # Delete nodes that are not related to an Execute Target event
    for n in graph.nodes():
        if n not in task_evts:
            graph.delete_node(n)
    # Get the root nodes to calculate the critical path from them
    root_nodes = []
    for n in graph.nodes_iter():
        if not graph.predecessors(n):
            root_nodes.append(n)

    # Stores critical path time for each graph node
    path_time = {}
    # Longest critical path time of all root nodes
    longest_path = -1
    # Id of the root node with the longest time
    root_longer = -1
    # Calculate critical path of all root nodes
    for root_node in root_nodes:
        calculate_critical_path(root_node, path_time, task_evts, graph)
        # Check if the current root node has a longer critical path
        if path_time[root_node] > longest_path:
            longest_path = path_time[root_node]
            root_longer = root_node

    # Get critical path edges
    edges_critical_path = []
    get_critical_path(root_longer, graph, edges_critical_path)

    # Get critical path tasks and edge events
    edges_evts = []
    critical_path_tasks = []
    critical_path_tasks.append(root_longer)
    for begin_node, end_node in edges_critical_path:
        critical_path_tasks.append(end_node)
        # Edge event common properties
        # Edge id: edge task begin + edge task end + dir name hash
        edge_evt = {
            "name": "edge",
            "cat": "Critical Path",
            "args": {},
            "id": begin_node + end_node + create_id(dir_name),
        }

        # Search for events related to begin and end nodes
        task_evt_end = task_evts[end_node]
        # Update pid and tid to join all events from the same execution and
        # separate task and com events
        task_evt_end.update({"pid": pid_head_process, "tid": pid_head_process})

        task_evt_begin = task_evts[begin_node]
        # Update pid and tid to join all events from the same execution and
        # separate task and com events
        task_evt_begin.update({"pid": pid_head_process, "tid": pid_head_process})

        # Create begin and end edge events
        edges_evts.append(
            create_edge_evt(edge_evt, task_evt_end, END_EVT, TIME_AS_BEGIN)
        )
        edges_evts.append(
            create_edge_evt(edge_evt, task_evt_begin, BEGIN_EVT, TIME_AS_END)
        )

    return critical_path_tasks, edges_evts


def get_critical_path_evts_single(
    critical_path_tasks,
    target_related_com_evts,
    edges_evts,
    task_evts,
    task_locations,
    pid_head_process,
):
    """Returns all critical path events of a single execution."""
    critical_path_evts_single = []
    for task in critical_path_tasks:
        # Get critical path task event
        critical_path_task_evt = task_evts[task]

        # Update task name to contain the source location hash
        mpi_tag = get_mpi_tag(critical_path_task_evt)
        critical_path_task_evt["name"] = "Task " + create_id(
            name=task_locations[mpi_tag], hash_len=4
        )

        # Update source location to appear on timeline
        critical_path_task_evt["args"]["Source Location:"] = task_locations[mpi_tag]
        critical_path_evts_single.append(critical_path_task_evt)

        # Get critical path communication events
        com_evts_same_id = get_evts_by_task_id(task, target_related_com_evts)
        com_evts_same_id_begin = get_evts_by_type(BEGIN_EVT, com_evts_same_id)
        # Merge begin and end communication events
        for begin_evt in com_evts_same_id_begin:
            evt_name = get_name_prefix(begin_evt)

            # Get the end event associated with the begin event
            com_evts_same_id_end = get_evts_by_type(END_EVT, com_evts_same_id, evt_name)
            end_evt = get_nearest_evt(begin_evt, com_evts_same_id_end)

            # Get begin event info
            com_evt = begin_evt
            # Update duration time and name to merge events
            # Update pid and tid to join all events from the same execution and
            # separate task and com events
            com_evt.update(
                {
                    "name": evt_name,
                    "pid": pid_head_process,
                    "tid": pid_head_process + 1,
                    "dur": (end_evt["ts"] + end_evt["dur"] - com_evt["ts"]),
                }
            )

            critical_path_evts_single.append(com_evt)

    critical_path_evts_single.extend(edges_evts)

    return critical_path_evts_single


def get_metadata_evts(pid_head_process, dir_name):
    """
    Returns a list of metadata events to name processes and threads on the
    timeline.
    """
    metada_evts = []
    # Directory name as a process
    metada_evts.append(
        {
            "name": "process_name",
            "ph": "M",
            "pid": pid_head_process,
            "args": {"name": dir_name},
        }
    )
    # Critical path events as a thread
    metada_evts.append(
        {
            "name": "thread_name",
            "ph": "M",
            "pid": pid_head_process,
            "tid": pid_head_process,
            "args": {"name": "Critical Path"},
        }
    )
    # Exchange events as a thread
    metada_evts.append(
        {
            "name": "thread_name",
            "ph": "M",
            "pid": pid_head_process,
            "tid": pid_head_process + 1,
            "args": {"name": "Exchange"},
        }
    )
    return metada_evts


def extract_ompc_critical_path_single(directory, critical_path_evts):
    """Extract the critical path of a single OMPCluster application."""
    pid_head_process = -1
    # Task-related event lists
    target_evts = []
    push_evts = []
    schedule_evts = []
    execute_evts = []
    com_evts = []

    # Get trace events and synchronize them
    for tracing_file in directory.glob("*.json"):
        tracing = open(tracing_file, "r").read()
        # Convert trace to JSON objects
        objs = json.loads(tracing)

        # Get lower ts to synchronize events
        lower_ts = get_lower_ts(objs["traceEvents"])

        for evt in objs["traceEvents"]:
            # Get task-related events
            if evt["ph"] == "X":
                evt["ts"] = evt["ts"] - lower_ts
                if evt["name"] in TARGET_EVTS:
                    target_evts.append(evt)
                elif evt["name"] == TASK_EVT:
                    execute_evts.append(evt)
                elif evt["name"] in COMM_EVTS:
                    com_evts.append(evt)
                elif evt["name"] == SCHEDULE_EVT:
                    schedule_evts.append(evt)
                elif evt["name"] == PUSH_NEW_EVT:
                    push_evts.append(evt)
            # Get events that name processes and threads
            elif evt["ph"] == "M" and evt["name"] != "process_sort_index":
                # Update the name of metadata events to app name and get
                # head process pid
                if evt["name"] == "process_name":
                    if evt["args"]["name"] == "Head Process":
                        pid_head_process = evt["pid"]

    # Relates execute event MPI Tag to task id
    task_ids = {}
    # Relates execute event MPI Tag to source location
    task_locations = {}
    # Communication events related to a target event
    target_related_com_evts = []
    for target_evt in target_evts:
        # Get schedule event related to a target event
        schedule_evt = get_schedule_evt(target_evt, schedule_evts)

        # Get push events related to a target event
        push_exec_evts = get_push_evts(target_evt, push_evts, ["Execute"])
        # Get executed event related to a push event
        push_exec_evt = get_nearest_evt(target_evt, push_exec_evts)

        # Relates MPI Tag to a task id
        mpi_tag = get_mpi_tag(push_exec_evt)
        task_ids[mpi_tag] = schedule_evt["args"]["task_id"]

        # Relates MPI Tag to a source location
        task_locations[mpi_tag] = target_evt["args"]["source_location"]

        # Get communication push events
        push_com_evts = get_push_evts(target_evt, push_evts, PUSH_COMM_EVTS)

        # Add worker communication events
        target_related_com_evts.extend(
            get_worker_com_evts(push_com_evts, com_evts, schedule_evt, pid_head_process)
        )

    # Relates task id to execute target events
    task_evts = get_task_evts_by_id(execute_evts, task_ids, pid_head_process)

    # Process graph dependencies to extract critical path
    for graph_file in directory.glob("*.dot"):
        critical_path_tasks, edges_evts = get_critical_path_evts(
            graph_file, task_evts, directory.name, pid_head_process
        )

        # Get critical path events of a single execution
        critical_path_evts_single = get_critical_path_evts_single(
            critical_path_tasks,
            target_related_com_evts,
            edges_evts,
            task_evts,
            task_locations,
            pid_head_process,
        )

        # Synchronize events of different executions
        lower_ts = get_lower_ts(critical_path_evts_single)
        for evt in critical_path_evts_single:
            evt["ts"] = evt["ts"] - lower_ts

        # Get metadata events to name processes and threads on the timeline
        metada_evts = get_metadata_evts(pid_head_process, directory.name)

        # Add critical path events of a single execution
        critical_path_evts.extend(metada_evts + critical_path_evts_single)

        # Only one graph is necessary
        break


def extract_ompc_critical_path(root_dir, output):
    """
    Extract the critical path into a single timeline from multiple OMPCluster
    applications divided into separate folders.
    """
    # Critical path events of all executions
    critical_path_evts = []

    # Each folder has a different OMPCluster execution
    root_dir_path = Path(root_dir)
    dir_list = []
    for obj in root_dir_path.iterdir():
        if obj.is_dir():
            dir_list.append(obj)

    # Check if there is only one execution
    if len(dir_list) == 0:
        dir_list = root_dir_path

    for directory in dir_list:
        extract_ompc_critical_path_single(directory, critical_path_evts)

    # Write json file
    with open(output, "w") as file:
        json.dump(critical_path_evts, file)
