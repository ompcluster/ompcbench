import os
import yaml

from datetime import datetime
from glob import glob
from itertools import product

from ompcbench import messages as msg
from ompcbench.lib.io import Parser, display_header, RESULTS_DIR
from ompcbench.lib.launcher import *
from ompcbench.util import Map
from ompcbench.check import verify_application_dir


class Experiment(object):
    """Controls the execution of an experiment with OmpCluster + TaskBench."""

    def __init__(self, options, config):
        self.parser = Parser(config, options)

    def create_output_dir(self):
        """Create directory where results will be saved."""
        if not self.parser.output:
            # Find a suitable directory name
            prefix = f"{RESULTS_DIR}/{self.parser.name}"
            dirs = glob(f"{prefix}_*")
            dirs = [int(d.removeprefix(prefix + "_")) for d in dirs]
            dnum = max(dirs or [-1]) + 1
            self.parser.output = f"{RESULTS_DIR}/{self.parser.name}_{dnum:03}"

        if not self.parser.options.dry_run:
            os.makedirs(self.parser.output)

    def create_matrix_cases(self):
        """Create all possible combinations from matrix."""
        keys = self.parser.keys
        vals = self.parser.vals

        # Cartesian product of all arrays in the matrix
        self.cases = [Map(zip(keys, item)) for item in product(*vals)]
        self.njobs = len(self.cases)

        return self.cases, self.njobs

    def run(self):
        """Launch jobs for this experiment."""
        if self.parser.options.verify:
            verify_application_dir(self.parser)

        self.create_output_dir()
        self.create_matrix_cases()
        display_header(self.parser, self.njobs)

        if self.parser.options.confirm or self.njobs > 32:
            msg.confirm(f"Proceed to launch {self.njobs} jobs?", abort=True)

        if not self.parser.options.dry_run:
            self.parser.logs.launch = open(f"{self.parser.output}/launch.log", "w")
            print(f"Date: {datetime.now()}", file=self.parser.logs.launch)
            # Used in analysis.py
            print(f"application={self.parser.type}", file=self.parser.logs.launch)

        succeeded = []
        launcher = get_launcher(self.parser)
        for index, case in enumerate(self.cases):
            ret = launcher.launch_job(case, index, self.parser)
            succeeded.append(ret)

        if self.parser.options.dry_run:
            msg.warn("No jobs launched! (dry run)")
            return

        self.parser.logs.launch.close()

        if not all(succeeded):
            msg.error("Some jobs failed to launch.")

        msg.success("Jobs launched successfully! 🎉")
