import os
import re
import sys
import numpy as np
import pandas as pd
import ompcbench.lib.pandas_util as pdu

from glob import glob
from pathlib import Path
from pandas.errors import EmptyDataError
from scipy import stats

from ompcbench import messages as msg
from ompcbench.lib.application import *
from ompcbench.util import Map


def bootstrap_metric(confidence, fn=np.mean):
    """Returns a closure that computes the bootstrap confidence interval."""

    def bootstrap(data):
        """Compute bootstrap."""
        return stats.bootstrap(
            (data,), fn, confidence_level=confidence
        ).confidence_interval

    return bootstrap


def build_dataframe_from_records(records, app):
    """Create DataFrame from raw data scavenged from job outputs."""

    data = (
        pd.DataFrame.from_records(records)
        .astype(
            {
                key: value
                for key, value in app.get_dataframe_column_types().items()
                if key in records[0].keys()
            }
        )
        .fillna("-")
        .sort_values(app.get_sort_values())
        .reset_index(drop=True)
    )

    return data


def process_dataframe(df, app, include_std=True, confidence=None):
    """Make the DataFrame more user friendly."""

    values = ["time"] + app.get_output_values()
    # Metrics follow the format (suffix, function)
    metrics = [("_mean", "mean")]
    if include_std:
        metrics += [("_std", "std")]
    if confidence is not None:
        metrics += [("_bootstrap", bootstrap_metric(confidence))]
    tag_metrics_with = lambda v: [(v + m[0], m[1]) for m in metrics]

    data = (
        # Only leave `time` and `sample` out of groupby
        df.groupby(df.columns.difference(values + ["sample"]).tolist())
        # Aggregate time by computing appropriate metrics and rename columns after aggregate
        .agg(
            dict(zip(values, map(tag_metrics_with, values)))
            | {"sample": [("samples", "count")]}
        )
    )

    # Convert columns MultiIndex to regular Index
    data.columns = data.columns.map(lambda x: x[1])
    # Reset index after previous groupby operation
    data = data.reset_index()

    if confidence is not None:
        for value in values:
            # Separate bootstrap tuple into two columns, `lo` and `hi`
            data[value + "_lo"] = data[value + "_bootstrap"].apply(lambda x: x[0])
            data[value + "_hi"] = data[value + "_bootstrap"].apply(lambda x: x[1])
            data.drop(columns=value + "_bootstrap", inplace=True)

    data = app.format_data(data)

    # Suffixes of columns that will be used as values after pivot
    suffixes = ["_mean", "_std", "_lo", "_hi"]
    # Values used in the DataFrame after the pivot
    index = [i for i in app.get_pivot_index() if i in data.columns.tolist()]
    columns = [i for i in app.get_pivot_columns() if i in data.columns.tolist()]
    results = [i for i in data.columns.tolist() if i.endswith(tuple(suffixes))]
    sorted_results = [
        r for r in [v + s for v in values for s in suffixes] if r in results
    ]

    data = (
        # Pivot the DataFrame with statistics as values
        data.pivot(index=index, columns=columns, values=results)
        # Add missing axis name after pivot
        .rename_axis(columns={None: "stats"})
        # Change the order that MultiIndex levels should be displayed
        .reorder_levels(columns + ["stats"], axis=1)
        # Sort column names
        .sort_index(axis=1)
        # Force the column order of the `stats` level to be the same as `values`
        .reindex(columns=sorted_results, level="stats")
    )

    return data


def failure_analysis(directory, df_raw, options):
    """Create a formatted DataFrame from the failure log and merges with."""

    fname = Path(directory) / "failure.log"

    msg.secho("\n\nFAILURE ANALYSIS", fg="green", bold=True)
    msg.secho("-" * 80, dim=True)

    if not fname.is_file():
        msg.warn("File 'failure.log' does not exist, skipping failure analysis.")
        return

    try:
        df = pd.read_csv(fname.name, header=None)
    except EmptyDataError as error:
        msg.success("No jobs failed.")
        return

    df = df.rename({0: "jobid", 1: "sample", 2: "code"}, axis=1)
    df = df.groupby("jobid").size()
    df.name = "failures"

    # Merge them together on the jobs that failed
    fail_df = pd.merge(df, df_raw, how="left", on="jobid")

    fail_df = fail_df.astype(
        {
            "failures": "string",
            "samples": "string",
        }
    )

    app = get_app_from_log(f"{directory}/launch.log")

    fail_df = app.format_data(fail_df, options.raw)
    fail_df["failures"] = fail_df[["failures", "samples"]].agg("/".join, axis=1)

    # Drop unused columns
    fail_df = fail_df.drop(["time_mean", "time_std", "samples"], axis=1)

    # Reorder columns
    fail_df = fail_df[
        [
            "jobid",
            *app.get_pivot_index(),
            *app.get_pivot_columns(),
            "failures",
        ]
    ]

    print(fail_df)


def get_app_from_log(fname):
    # Get application name written in log
    with open(fname, "r") as stream:
        lines = stream.readlines()
        name = lines[1].replace("application=", "").replace("\n", "")

    return get_uninitialized_app(name)


def run_analysis(directory, options):
    """Run data analysis on elapsed times and output it."""

    fnames = list(Path(directory).glob(f"./*.out"))

    if not len(fnames) > 0:
        msg.fatal(f"No files to parse were found under `{directory}`.")

    app = get_app_from_log(f"{directory}/launch.log")

    # Load all samples from every file. Records is a list of dictionaries, each
    # dict correspond to a single execution sample.
    records = []
    for fname in fnames:
        records += app.parse_file(fname, options.time_source)

    # Create DataFrame from records and to basic pre-processing
    df = build_dataframe_from_records(records, app)

    # Save a copy of the raw DataFrame
    df_raw = df

    # Pivot and improve DataFrame format
    if not options.raw:
        df = process_dataframe(df, app, options.std, options.bootstrap)

    # Custom keyword arguments for each format
    custom_kwargs = {"txt": {"float_format": "%8f"}}

    if options.output is None:
        msg.secho("\nRUNTIME ANALYSIS", fg="green", bold=True)
        msg.secho("-" * 80, dim=True)

    # Write DataFrame to output file
    pdu.save_dataframe(df, file=options.output, **custom_kwargs)

    # Perform failure analysis
    if options.failure:
        failure_analysis(directory, df_raw, options)
