import sys
import traceback as tb

from click import echo, secho, style, confirm

GOOD = style("SUCCESS ✅", fg="green", bold=True)
BAD = style("FAILURE ❌", fg="red", bold=True)


def _message(title, msg, err=False, nl=True, **style_kwargs):
    """
    Display a stylized message.
    """
    title = style(title.upper(), **style_kwargs)
    L, R = style("[", dim=True), style("]", dim=True)
    echo(f"{L}{title}{R} {msg}", err=err, nl=True)


def list_writer(prefix="-", error=False, **styleopts):
    """
    Returns a clojure that writes list elements.
    """

    def writer(msg):
        p = style(prefix, dim=True)
        echo(f"{p} {style(str(msg), **styleopts)}", err=error)

    return writer


def table_writer(prefix="-", error=False, width=20, **styleopts):
    """
    Returns a clojure that writes table rows.
    """

    def writer(left, right):
        left = str(left)
        right = style(str(right), **styleopts)
        p = style(prefix, dim=True)
        echo(f"{p} {left:{width}} {right}", err=error)

    return writer


def fatal(msg, traceback=True, code=1):
    """
    Display an error message and then die.
    """
    if isinstance(msg, Exception):
        error_msg = f"{type(msg).__name__}: {str(msg)}"
        _message("ERR", error_msg, err=True, bg="magenta")
        if traceback:
            tb.print_tb(msg.__traceback__)
    else:
        _message("ERR", msg, err=True, bg="magenta")

    sys.exit(code)


def error(msg, traceback=False):
    """
    Display an error message.
    """
    if isinstance(msg, Exception):
        error_msg = f"{type(msg).__name__}: {str(msg)}"
        _message("ERR", error_msg, err=True, fg="red", bold=True)
        if traceback:
            tb.print_tb(msg.__traceback__)
    else:
        _message("ERR", msg, err=True, fg="red", bold=True)
    sys.exit(1)


def warn(msg):
    """
    Display a warning message.
    """
    _message("WRN", msg, err=False, fg="yellow", bold=True)


def info(msg):
    """
    Display an information message.
    """
    _message(" ! ", msg, err=False, fg="cyan", bold=True)


def success(msg):
    """
    Display a success message.
    """
    _message(f" ✔ ", msg, err=False, fg="green", bold=True)


def debug(msg):
    """
    Display a debug message.
    """
    _message("DBG", msg, err=False, fg="white", bold=True)
