import sys
import os
import re

from git import Repo
from schema import SchemaError
from click import echo, style

from ompcbench import __version__, messages as msg
from ompcbench.lib.io import Parser
from ompcbench.schema import ConfigSchema as schema
from ompcbench.version import version_compatible
from ompcbench.util import run_cmd


def run_check(raw_config, options):
    """
    Check if the configuration file is valid.
    """
    writer = msg.table_writer()
    config = {}

    try:
        config = schema.validate(raw_config)
        writer("Configuration", msg.GOOD)
    except SchemaError as error:
        msg.echo(error)
        msg.error("Invalid YAML configuration!")

    try:
        validate_version(writer, config.version)
        if options.verify:
            validate_application(writer, config)
            validate_images(writer, config.environment)
    except (RuntimeError, FileNotFoundError) as error:
        msg.error(str(error))

    if options.verbose:
        from pprint import pprint

        pprint(config)


def validate_version(writer, version_str):
    """
    Validate YAML configuration version with current OMPC Bench version.
    """
    if not version_compatible(version_str):
        raise RuntimeError(
            f"Expected OMPC Bench version {__version__} but got {version_str}"
        )
    writer("Version", msg.GOOD)


def validate_application(writer, config):
    """
    Validate application directory and binaries.
    """
    parser = Parser(config)
    verify_application_dir(parser)
    writer(f"{parser.type.capitalize()} repo", msg.GOOD)

    for strat, values in config.environment.items():
        if strat == "all":
            continue

        bin = values.application.binary or config.environment.all.application.binary
        verify_application_bin(parser.dir, bin)
        writer(f"{parser.type.capitalize()}: {strat}", msg.GOOD)


def verify_application_dir(parser):
    """
    Validate that the application directory and hash are valid.
    """
    expected = parser.hash

    if not os.path.isdir(parser.dir):
        raise FileNotFoundError(f"{parser.type.capitalize()} directory not found!")

    if not expected:
        return

    repo = Repo(parser.dir)
    commit = str(repo.head.commit)

    if not commit.startswith(expected):
        raise RuntimeError(
            f"Expected {parser.type.capitalize()} commit '{expected[:7]}' got '{commit[:7]}'"
        )


def verify_application_bin(dir, binary):
    """
    Validate that an application executable exists on disk.
    """
    bin_path = os.path.join(dir, binary)

    if not os.path.isfile(bin_path):
        raise FileNotFoundError(
            f"{parser.type.capitalize()} binary not found: {bin_path}"
        )


def validate_images(writer, environment):
    """
    Validate the image of each strategy.
    """
    for strat, values in environment.items():
        if values.image:
            verify_container_image(values.image)
            writer(f"Image: {strat}", msg.GOOD)


def verify_container_image(image):
    """
    Validate that the container image path and hash are valid.
    """
    expected = image.ompc_hash if image else None

    if not image:
        return

    if not os.path.isfile(image.path):
        raise FileNotFoundError(f"Container image not found: {image.path}")

    if not expected:
        return

    proc = run_cmd(["singularity", "exec", image.path, "clang", "--version"])

    if proc.code != 0:
        raise RuntimeError(
            "Failed to obtain OmpCluster version\n" + f"{proc.stderr.decode('utf-8')}"
        )

    version = proc.stdout.decode("utf-8").split("\n")[0]
    match = re.match(r"^.+ \(\S+ (\w+)\)$", version)

    if not match:
        raise RuntimeError("Could not match OmpCluster commit version")

    commit = match.group(1)
    if not commit.startswith(image.ompc_hash):
        raise RuntimeError(
            f"Expected OmpCluster commit '{expected[:7]}' got '{commit[:7]}' ({image.path})"
        )
