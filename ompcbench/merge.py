import orjson

import ompcbench.messages as msg
from ompcbench.filter import filter_and_rename_evts


def merge_traces(
    ompc_path, ompc_files, ompt_files=[], *, developer=False, synchronize=True
):
    """
    Merge OMPC and OmpTracing timelines with options to filter and synchronize
    events.
    """
    ompc_evts = []
    ompt_evts = []
    min_ts_by_pid = {}

    # Merge events from OMPC traces
    for file in ompc_files:
        local_timeline = orjson.loads(file.read())

        if "traceEvents" not in local_timeline:
            msg.fatal(f"File `{file.name}` does not seem to be a trace.")

        evts = local_timeline["traceEvents"]

        # Synchronize events across timelines
        if synchronize:
            # Find the smaller timestamp among events
            min_ts = min(evt["ts"] for evt in evts if evt["ph"] == "X")
            # Shift all events timestamp back by the minimum timestamp
            for evt in local_timeline["traceEvents"]:
                if evt["ph"] == "X":
                    evt["ts"] = evt["ts"] - min_ts
                min_ts_by_pid[evt["pid"]] = min_ts

        ompc_evts.extend(evts)

    # Add dependencies on execute target events from head nodes to worker nodes
    if not developer:
        ompc_evts = filter_and_rename_evts(ompc_evts, ompc_path)

    # Merge events from OMPT traces
    for file in ompt_files:
        local_evts = orjson.loads(file)

        if synchronize:
            min_ts = min(evt["ts"] for evt in local_evts if evt["ph"] == "B")
            for evt in local_evts:
                evt["ts"] = evt["ts"] - min_ts

        ompt_evts.extend(local_evts)

    # Return unified timeline containing both OMPC and OMPT events
    return {
        "traceEvents": [
            *ompt_evts,
            *ompc_evts,
        ]
    }
