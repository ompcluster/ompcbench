import orjson
import glob
import pygraphviz as pgv

from ompcbench.util import (
    check_files,
    create_edge_dict,
    create_edge_evt,
    create_id,
    get_begin_evts,
    get_end_evts,
    get_evts_same_mpi_tag,
    get_evts_same_name_prefix,
    get_evts_same_thread,
    get_inner_evt,
    get_mpi_tag,
    get_name_prefix,
    get_nearest_evt,
    get_push_assoc_evts,
    get_task_node_name,
    BEGIN_EVT,
    END_EVT,
    TIME_AS_BEGIN,
    TIME_AS_END,
    NEAREST_TO_END,
)

from ompcbench import messages as msg

# Global Constants
# ==============================================================================

# User events of OMPC trace that will be filtered.
# Begin and End events are separeted to facilitate merge processing.
USER_EVTS = [
    "__tgt_target_nowait_mapper",
    "__tgt_target_mapper",
    "__tgt_target_data_begin_nowait_mapper",
    "__tgt_target_data_begin_mapper",
    "__tgt_target_data_end_nowait_mapper",
    "__tgt_target_data_end_mapper",
    "Event System Execution",
    "Execute / Target Execution",
    "VarName",
]
USER_BEGIN_EVTS = [
    "Exchange / Begin",
    "Alloc / Begin",
    "Execute / Begin",
    "Submit / Begin",
    "Delete / Begin",
    "Retrieve / Begin",
]
USER_END_EVTS = [
    "Exchange / End",
    "Alloc / End",
    "Execute / End",
    "Submit / End",
    "Delete / End",
    "Retrieve / End",
]

# List of events associated with user variable events of OMPC trace.
""" 
    The user variable event incorporate the associated event information and 
    contains the variable name and its code location.
    pushNewEvent is a special case and will be used to find the corresponding 
    asynchronous events.
"""
USER_VAR_ASSOC_EVTS = ["Alloc / Begin", "Delete / Begin", "pushNewEvent"]
USER_VAR_ASSOC_END_EVTS = ["Alloc / End", "Delete / End"]

# List of events associated with pushNewEvent.
"""
    Since pushNewEvent does not represent the user variable, it is necessary to
    look for an event with the type of this list.
"""
PUSH_ASSOC_EVTS = [
    "Exchange / Begin",
    "Execute / Begin",
    "Submit / Begin",
    "Retrieve / Begin",
    "Exchange / End",
    "Execute / End",
    "Submit / End",
    "Retrieve / End",
]

# List of associated events tuples.
# The tuple's second event must be removed when associated with the first.
ASSOC_EVTS_LIST = [
    ("__tgt_target_data_begin_nowait_mapper", "__tgt_target_data_begin_mapper"),
    ("__tgt_target_nowait_mapper", "__tgt_target_mapper"),
    ("__tgt_target_kernel_nowait", "__tgt_target_kernel"),
    ("__tgt_target_data_end_nowait_mapper", "__tgt_target_data_end_mapper"),
]

# Association between OMPC event and its new name in filtered trace.
RENAME_EVTS = {
    "__tgt_target_nowait_mapper": "Target Nowait",
    "__tgt_target_kernel_mapper": "Target Nowait",
    "__tgt_target_mapper": "Target",
    "__tgt_target_data_begin_nowait_mapper": "Target Enter Nowait",
    "__tgt_target_data_begin_mapper": "Target Enter",
    "__tgt_target_data_end_nowait_mapper": "Target Exit Nowait",
    "__tgt_target_data_end_mapper": "Target Exit",
    "Event System Execution": "OMPC Runtime Execution",
    "Exchange": "Forward",
    "Exchange / Begin": "Forward / Begin",
    "Exchange / End": "Forward / End",
}

# Task event colors on timeline.
# Colors will be chosen from last to first.
TASK_COLORS = [
    "cq_build_running",  # Paris Daisy
    "cq_build_passed",  # Sulu
    "cq_build_attempt_runnig",  # Wattle
    "cq_build_attempt_passed",  # Lima
    "good",  # Japanese Laurel
    "bad",  # Pirate Gold
    "olive",  # Olive
    "thread_state_uninterruptible",  # Turkish Rose
    "thread_state_iowait",  # Pizazz
    "thread_state_running",  # De York
    "thread_state_unknown",  # Tan
    "light_memory_dump",  # Dark Blue
    "rail_idle",  # Tangerine
    "rail_load",  # Gossamer
    "background_memory_dump",  # Pacific Blue
    "detailed_memory_dump",  # Flirt
    "thread_state_runnable",  # Polo Blue
    "heap_dump_child_node_arrow",  # Tenn
    "vsync_highlight_color",  # Blue
    "startup",  # Turbo
    "rail_response",  # Dodger Blue
    "cq_build_failed",  # Apricot
    "yellow",  # Yellow
]
DEFAULT_COLOR = "yellow"

# Event Filter Functions
# ==============================================================================


def is_main_evt(evt, main_evt_name) -> bool:
    """
    Returns True if the event is a main event from associated events and False
    otherwise.
    Exchange/Submit is a specific case and the Destination must be checked.
    """
    return (
        evt["args"]["Location"] == "Destination" and main_evt_name == evt["name"]
        if get_name_prefix(evt) == "Exchange"
        else evt["name"] == main_evt_name
    )


def is_sec_evt(evt, sec_evt_name) -> bool:
    """
    Returns True if the event is a secondary event from associated events and
    False otherwise.
    """
    return evt["name"] == sec_evt_name


def get_non_exec_evts(evts):
    """Returns a list of events whose name prefix is different from Execute."""
    return [e for e in evts if get_name_prefix(e) != "Execute"]


# Dependencies Functions
# ==============================================================================


def add_com_dependencies(all_evts, begin_evts, end_evts):
    """
    Returns edge events that represent the timeline dependencies between the
    begin and end communication events.
    """
    edge_evts = []

    # Base edge event info
    edge_evt = create_edge_dict(cat="Communication")

    # Create a single dependency for each begin-end event association
    for begin_evt in begin_evts:
        # Get nearest associated end event
        end_evts_same_name_prefix = get_evts_same_name_prefix(begin_evt, end_evts)
        end_evts_same_name_prefix_by_tag = get_evts_same_mpi_tag(
            begin_evt, end_evts_same_name_prefix
        )
        end_evt = get_nearest_evt(begin_evt, end_evts_same_name_prefix_by_tag)

        # Create edge event unique id
        edge_evt["id"] = create_id(get_name_prefix(begin_evt), begin_evt)

        # Create begin and end edge events
        edge_evts.append(create_edge_evt(edge_evt, begin_evt, BEGIN_EVT, TIME_AS_END))
        edge_evts.append(create_edge_evt(edge_evt, end_evt, END_EVT, TIME_AS_BEGIN))

    all_evts.extend(edge_evts)


def get_exec_to_exec_target_edge_evts(exec_target_evts, exec_evt_by_tag, metadata_evts):
    """
    Returns edge events that represent the timeline dependencies between the
    execution events of the head and worker process threads.
    """
    edge_evts = []

    # Get metadata events to name dependencies
    # Associate pid with process name
    process_names = {}
    for metada_evt in metadata_evts:
        process_names[metada_evt["pid"]] = metada_evt["args"]["name"]

    # Create a single dependency for each execute-target event association
    for exec_target_evt in exec_target_evts:
        # Get target event associated
        exec_evt = exec_evt_by_tag[get_mpi_tag(exec_target_evt)]

        # Base edge event info
        edge_evt = create_edge_dict(
            cat=process_names[exec_target_evt["pid"]],
            id=create_id("ExcBegin", exec_evt),
        )

        # Create dependency between the begin of events
        edge_evts.append(create_edge_evt(edge_evt, exec_evt, BEGIN_EVT, TIME_AS_BEGIN))
        edge_evts.append(
            create_edge_evt(edge_evt, exec_target_evt, END_EVT, TIME_AS_BEGIN)
        )

        # Create dependency between the end of events
        # Create end edge event unique id
        edge_evt["id"] = create_id("ExcEnd", exec_evt)
        edge_evts.append(
            create_edge_evt(edge_evt, exec_target_evt, BEGIN_EVT, TIME_AS_END)
        )
        edge_evts.append(create_edge_evt(edge_evt, exec_evt, END_EVT, TIME_AS_END))

    return edge_evts


def get_task_edge_evts(exec_evt_by_task_id, graph_file):
    """Get edge events from task-to-task dependencies on the head node."""
    edge_evts = []

    # Load graph file
    graph = pgv.AGraph(graph_file, strict=False, directed=True)

    for node in graph.nodes():
        # Only execution tasks are used
        if node in exec_evt_by_task_id:
            for edge in graph.edges_iter(node):
                (begin_node, end_node) = edge

                # Ensure that end_node is also an execution task
                if begin_node == node and end_node in exec_evt_by_task_id:
                    edge_evt = create_edge_dict(
                        cat="Tasks Dependencies",
                        id=begin_node + end_node + create_id(graph_file.name),
                    )

                    # Get begin and end node execution events
                    task_evt_end = exec_evt_by_task_id[end_node]
                    task_evt_begin = exec_evt_by_task_id[begin_node]

                    # Create begin and end edge events
                    edge_evts.append(
                        create_edge_evt(edge_evt, task_evt_end, END_EVT, TIME_AS_BEGIN)
                    )
                    edge_evts.append(
                        create_edge_evt(
                            edge_evt, task_evt_begin, BEGIN_EVT, TIME_AS_END
                        )
                    )
    return edge_evts


# Event Modification Functions
# ==============================================================================


def set_colors_by_source_loc(
    exec_target_evts, color_by_source_loc, source_loc_by_tag, task_id_by_tag
):
    """
    Set colors for the Execute Target events according to their source location.
    """
    for exec_target_evt in exec_target_evts:
        # Assign a color to a source location if it does not already exists
        if source_loc_by_tag[get_mpi_tag(exec_target_evt)] not in color_by_source_loc:
            if TASK_COLORS:
                color = TASK_COLORS.pop()
            else:
                color = DEFAULT_COLOR

            color_by_source_loc[source_loc_by_tag[get_mpi_tag(exec_target_evt)]] = color

        # Set event color
        exec_target_evt["cname"] = color_by_source_loc[
            source_loc_by_tag[get_mpi_tag(exec_target_evt)]
        ]

        # Add source location info to Execute Target event
        exec_target_evt["args"]["source_location"] = source_loc_by_tag[
            get_mpi_tag(exec_target_evt)
        ]

        # Change event name to task id
        exec_target_evt["name"] = "Task " + str(
            task_id_by_tag[get_mpi_tag(exec_target_evt)]
        )


def set_task_id(
    exec_evt,
    target_evts,
    schedule_evts,
    source_loc_by_tag,
    task_id_by_tag,
    exec_evt_by_task_id,
):
    """Set task id to an execution event from head node."""

    # Get associated target event
    target_evts_same_thread = get_evts_same_thread(exec_evt, target_evts)
    target_evt = get_nearest_evt(exec_evt, target_evts_same_thread)

    # Associate the execution event's MPI Tag to its source location
    source_loc_by_tag[get_mpi_tag(exec_evt)] = target_evt["args"]["source_location"]

    # Get associated schedule event
    schedule_inner_evt = get_inner_evt(target_evt, schedule_evts)

    # Associate the execution event's MPI Tag to its task id
    task_id_by_tag[get_mpi_tag(exec_evt)] = schedule_inner_evt["args"]["task_id"]

    # Set task id to execution event
    exec_evt["args"]["task_id"] = schedule_inner_evt["args"]["task_id"]

    # Associate task id to execution event
    exec_evt_by_task_id[get_task_node_name(exec_evt["args"]["task_id"])] = exec_evt


def remove_assoc_evts(ompc_evts) -> list:
    """
    Remove associated events (main event and secondary event), two events that
    always appear together, but the main event is sufficient for user trace.
    """
    # Remove secondary events by iterating over main events
    for main_evt_name, sec_evt_name in ASSOC_EVTS_LIST:
        # Separating associated events
        main_evts = [e for e in ompc_evts if is_main_evt(e, main_evt_name)]
        sec_evts = [e for e in ompc_evts if is_sec_evt(e, sec_evt_name)]

        # Secondary event must be removed when associated with the main event
        for main_evt in main_evts:
            # Get secondary events from the same pid/tid
            sec_assoc_evts = get_evts_same_thread(main_evt, sec_evts)

            # Check if the main event has an associated event
            if sec_assoc_evts is not None:
                # Remove the nearest secondary event from the trace
                nearest_evt = get_nearest_evt(main_evt, sec_assoc_evts)
                if nearest_evt is not None:
                    ompc_evts.remove(nearest_evt)
                    sec_evts.remove(nearest_evt)

    return ompc_evts


def add_user_var_info(
    user_var_evts, user_var_assoc_evts, user_var_assoc_end_evts, push_assoc_evts
):
    """
    Associate each user variable event with its representative event in the OMPC
    timeline and incorporate its information.
    """
    # Sort user variable lists to handle the case of events with same MPI Tag
    # The first event in the timeline must be associated with the nearest event
    user_var_evts.sort(key=lambda e: e["ts"])

    # Incorporate associated event information in user variable event
    for user_var_evt in user_var_evts:
        # Get associated events from the same pid/tid
        assoc_evts = get_evts_same_thread(user_var_evt, user_var_assoc_evts)

        # Search the nearest associated event from the trace
        assoc_evt = get_nearest_evt(user_var_evt, assoc_evts)

        # PushNewEvent is associated with asynchronous user variable events
        # These events are associated with a user variable event
        if assoc_evt["name"] == "pushNewEvent":
            # Get push associated events from the same pid/tid
            push_assoc_evts_same_thread = get_evts_same_thread(
                assoc_evt, push_assoc_evts
            )

            # Search for push associated events with same MPI Tag and event type
            push_assoc_evts_same_tag = get_push_assoc_evts(
                assoc_evt, push_assoc_evts_same_thread
            )

            # Search for push associated begin event
            push_assoc_begin_evts = get_begin_evts(push_assoc_evts_same_tag)

            # Search for the nearest associated begin event
            assoc_begin_evt = get_nearest_evt(user_var_evt, push_assoc_begin_evts)

            # Search for end event with same MPI Tag and that starts after begin
            # event
            push_assoc_end_evts = get_end_evts(
                push_assoc_evts_same_tag, assoc_begin_evt
            )

            # Search for the nearest associated end event
            assoc_end_evt = get_nearest_evt(user_var_evt, push_assoc_end_evts)
        else:
            # Get associated end events from the same pid/tid
            assoc_end_evts_same_thread = get_evts_same_thread(
                user_var_evt, user_var_assoc_end_evts
            )
            assoc_end_evts = get_evts_same_name_prefix(
                assoc_evt, assoc_end_evts_same_thread
            )

            # Search the nearest associated end event from the trace
            assoc_end_evt = get_nearest_evt(
                user_var_evt, assoc_end_evts, NEAREST_TO_END
            )

            # Get begin event
            assoc_begin_evt = assoc_evt
            # Remove end event so it won't be associated again
            user_var_assoc_end_evts.remove(assoc_end_evt)

        # Remove event so it won't be associated with another user_var event
        user_var_assoc_evts.remove(assoc_evt)

        # Add associated event args and timeline position in user variable event
        user_var_evt["name"] = user_var_evt["args"]["identifier"]

        # User variable event must start immediately before the associated event
        # This ensures that it appears above the associated event on the
        # timeline
        user_var_evt["ts"] = assoc_begin_evt["ts"] - 0.000001
        user_var_evt["dur"] = (
            assoc_end_evt["ts"] - assoc_begin_evt["ts"]
        ) + assoc_end_evt["dur"]

        user_var_evt["args"].pop("identifier", None)


def merge_evts(
    user_begin_evts,
    user_end_evts,
    head_process_pid,
    target_evts,
    schedule_evts,
    source_loc_by_tag,
    task_id_by_tag,
    exec_evt_by_task_id,
    exec_evt_by_tag,
) -> list:
    """
    Merge user begin events and user end events with the same pid / tid to
    decrease the number of events in the Head Process.
    """
    evts_merged = []
    begin_evts_to_remove = []

    # Get Head Process begin events
    user_begin_head_evts = [e for e in user_begin_evts if e["pid"] == head_process_pid]

    # For each user begin event try to find an associated end event to merge
    for user_begin_evt in user_begin_head_evts:
        # Get associated end events from the same pid/tid
        assoc_evts_same_thread = get_evts_same_thread(user_begin_evt, user_end_evts)
        assoc_evts_same_thread_by_tag = get_evts_same_mpi_tag(
            user_begin_evt, assoc_evts_same_thread
        )
        assoc_end_evts = get_evts_same_name_prefix(
            user_begin_evt, assoc_evts_same_thread_by_tag
        )

        # Search the nearest associated end event from the trace
        assoc_end_evt = get_nearest_evt(user_begin_evt, assoc_end_evts)

        # Merge begin and end events
        evt_merged = user_begin_evt
        evt_merged["name"] = get_name_prefix(user_begin_evt)

        # The merged event must begin at the same time as the begin event and
        # end with end event
        evt_merged["dur"] = (
            assoc_end_evt["ts"] + assoc_end_evt["dur"] - user_begin_evt["ts"]
        )
        evts_merged.append(evt_merged)

        if evt_merged["name"] == "Execute":
            # Set task id for head node execution events
            set_task_id(
                evt_merged,
                target_evts,
                schedule_evts,
                source_loc_by_tag,
                task_id_by_tag,
                exec_evt_by_task_id,
            )
            # Separate execute event by MPI Tag to add dependencies later
            exec_evt_by_tag[get_mpi_tag(evt_merged)] = evt_merged

        # Remove end events that are merged
        user_end_evts.remove(assoc_end_evt)
        # Get begin events that are merged
        begin_evts_to_remove.append(user_begin_evt)

    # Remove begin events that are merged
    for user_begin_evt in begin_evts_to_remove:
        user_begin_evts.remove(user_begin_evt)

    return evts_merged


# Main Functions
# ==============================================================================


def filter_and_rename_evts(ompc_evts, ompc_path=None) -> list:
    """
    Filter and rename OMPC events to improve the visualization for the user.
    """
    # Events after filtering
    updated_evts = []

    # Separate events to increase performance
    exec_target_evts = []
    target_evts = []
    schedule_evts = []
    user_begin_evts = []
    user_end_evts = []
    user_var_evts = []
    user_var_assoc_evts = []
    user_var_assoc_end_evts = []
    push_assoc_evts = []
    process_metadata_evts = []
    exec_evt_by_tag = {}
    exec_evt_by_task_id = {}

    # Task info separated by tag
    source_loc_by_tag = {}
    task_id_by_tag = {}
    color_by_source_loc = {}

    head_process_pid = -1

    # Get user events and search for the user variable events and their
    # associated events
    for event in ompc_evts:
        if event["ph"] == "M":
            updated_evts.append(event)
            # Get Head Process pid
            if event["name"] == "process_name":
                if event["args"]["name"] == "Head Process":
                    head_process_pid = event["pid"]
                process_metadata_evts.append(event)
        else:
            if event["name"] in USER_EVTS:
                updated_evts.append(event)
            elif event["name"] in USER_BEGIN_EVTS:
                user_begin_evts.append(event)
            elif event["name"] in USER_END_EVTS:
                user_end_evts.append(event)
            if event["name"] in USER_VAR_ASSOC_EVTS:
                user_var_assoc_evts.append(event)
            elif event["name"] in USER_VAR_ASSOC_END_EVTS:
                user_var_assoc_end_evts.append(event)
            elif event["name"] in PUSH_ASSOC_EVTS:
                push_assoc_evts.append(event)
            elif event["name"] == "VarName":
                user_var_evts.append(event)
            elif (
                event["name"] == "__tgt_target_nowait_mapper"
                or event["name"] == "__tgt_target_kernel_nowait"
                or event["name"] == "__tgt_target_kernel"
            ):
                target_evts.append(event)
            elif event["name"] == "Schedule Task":
                schedule_evts.append(event)
            elif event["name"] == "Execute / Target Execution":
                exec_target_evts.append(event)

    # Remove associated user events to not duplicate information
    remove_assoc_evts(updated_evts)
    remove_assoc_evts(user_begin_evts)
    remove_assoc_evts(user_end_evts)

    # Associated user variable event with its representative event on the
    # timeline
    if user_var_evts:
        add_user_var_info(
            user_var_evts,
            user_var_assoc_evts,
            user_var_assoc_end_evts,
            push_assoc_evts,
        )

    if head_process_pid != -1:
        # Add merged events
        updated_evts.extend(
            merge_evts(
                user_begin_evts,
                user_end_evts,
                head_process_pid,
                target_evts,
                schedule_evts,
                source_loc_by_tag,
                task_id_by_tag,
                exec_evt_by_task_id,
                exec_evt_by_tag,
            )
        )

        if ompc_path is not None:
            # Get the task graph which is in the same folder as the traces
            graph_list = list(ompc_path.glob("*.dot"))
            if graph_list:
                graph_file = graph_list[0]
                # Add task dependencies on the head node
                updated_evts.extend(get_task_edge_evts(exec_evt_by_task_id, graph_file))
            else:
                msg.warn(
                    "To add task dependencies to the timeline, put the graph file in the same folder as the traces."
                )

        # Dependencies from the head node's Execute event to the worker node's Execute Target event
        updated_evts.extend(
            get_exec_to_exec_target_edge_evts(
                exec_target_evts, exec_evt_by_tag, process_metadata_evts
            )
        )

        # Set colors for worker node task events according to source location
        set_colors_by_source_loc(
            exec_target_evts, color_by_source_loc, source_loc_by_tag, task_id_by_tag
        )

        # Separate begin and end non-execution events
        user_begin_evts = get_non_exec_evts(user_begin_evts)
        user_end_evts = get_non_exec_evts(user_end_evts)

        # Add timeline dependencies between them
        add_com_dependencies(updated_evts, user_begin_evts, user_end_evts)

    # Add events that are impossible to merge
    updated_evts.extend(user_begin_evts)
    updated_evts.extend(user_end_evts)

    # Rename events
    for evt in updated_evts:
        if evt["name"] in RENAME_EVTS:
            evt["name"] = RENAME_EVTS[evt["name"]]

    return updated_evts


def filter_ompc_traces(ompc_prefix, output):
    """
    Merge OMPC and OmpTracing timelines with options to filter and synchronize
    events.
    """
    # Checking files
    check_files(ompc_prefix)

    # Merge OMPC timelines
    ompc_evts = []
    for ompc_file in glob.glob(ompc_prefix + "*"):
        with open(ompc_file, "r") as ompc_tracing:
            # Convert OMPC trace to JSON objects
            ompc_objs = orjson.loads(ompc_tracing.read())
            if "traceEvents" in ompc_objs:
                # Filter and rename events
                ompc_evts.extend(filter_and_rename_evts(ompc_objs["traceEvents"]))

    # Creating new JSON timeline
    output_objs = orjson.loads('{"traceEvents":[]}')
    output_objs["traceEvents"].extend(ompc_evts)

    # Writing JSON timeline
    if output is None:
        output_file = open("tracing.json", "wb")
    else:
        output_file = open(output, "wb")
    output_file.write(orjson.dumps(output_objs))
    output_file.close()
