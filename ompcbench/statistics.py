import os
import sys
import warnings
import pandas as pd
import ompcbench.lib.pandas_util as pdu

from pathlib import Path
from ompcbench.lib.trace import Trace
from ompcbench.util import is_from_same_thread
from pandas.errors import PerformanceWarning

# Global Constants
# ==============================================================================

# Name of all event types in the OmpCluster runtime.
# NOTE: This list should be kept in sync with the addition of new events.
EVENTS = [
    "Debug",
    "Alloc",
    "Delete",
    "Retrieve",
    "Submit",
    "PackedRetrieve",
    "PackedSubmit",
    "PackedExchange",
    "PackedBcast",
    "Broadcast",
    "DynBcast",
    "Exchange",
    "Execute",
    "Exit",
]

# Overhead Constants
# ==============================================================================

# Some strings to be searched in the events
EVT_SYS = "Event System Execution"
EVT_HDL = "Event Handler Execution"
GAT_THD = "Gate Thread Execution"
SCHEDULER = "Schedule"

# Default the columns in the overhead table should be displayed.
# This is customizable with the option `-c/--cols <string>`.
# TODO: This is not working on `run_overhead_stats`
OVERHEAD_COLUMN_ORDER = [
    "file",
    "process",
    "pid",
    "thread",
    "tid",
    "wall_time",
    "startup",
    "shutdown",
    "scheduler",
    "idle",
    "overhead",
]

# Which type each column in the overhead table should be converted to.
OVERHEAD_COLUMN_TYPE = {
    "file": "string",
    "process": "category",
    "thread": "category",
    "pid": "int32",
    "tid": "int32",
}

# How each column in the overhead table should be aggregated.
# TODO: Review this carefully.
OVERHEAD_COLUMN_AGG = {
    "file": "first",
    "process": "first",
    "pid": "nunique",
    "thread": "first",
    "tid": "nunique",
    "wall_time": "max",
    "startup": ["sum", "mean"],
    "shutdown": ["sum", "mean"],
    "scheduler": ["sum", "mean"],
    "idle": ["sum", "mean"],
    "overhead": ["min", "max", "mean"],
}

# Overhead statistics
# ==============================================================================


def run_overhead_stats(files, options):
    """Run the statistics analysis."""
    # Compute overhead and obtain a DataFrame
    df = compute_overhead(files, aggregate_data=options.agg)

    # Select which columns to display
    if options.cols is not None:
        df = df[options.cols]

    if options.output is None:
        options.output = sys.stdout

    # Write data to output file
    pdu.save_dataframe(df, file=options.output)


def compute_overhead(files, *, aggregate_data=False):
    """Run overhead analysis on multiple merged trace files."""
    records = []
    # Compute overhead for each input file
    for file in files:
        fname = os.path.basename(file.name.rstrip(".json"))
        trace = Trace.from_file(file)
        records.extend(compute_overhead_from_file(fname, trace))

    # Create dataframe from records obtained from each file
    df = pd.DataFrame.from_records(records).astype(OVERHEAD_COLUMN_TYPE)
    # Compute the overhead (in %) using the thread wall-time
    df["overhead"] = 100 * (df.overhead / df.wall_time)

    # Aggregate data
    if aggregate_data:
        df = df.groupby(["file"]).agg(OVERHEAD_COLUMN_AGG)
        df = df.drop(["process", "thread"], axis=1).rename(
            columns={
                "pid": "nprocs",
                "tid": "nthreads",
            }
        )

    return df


def compute_overhead_from_file(fname, trace):
    """Obtain overhead-related statistics from DataFrame."""

    records = []

    # Calculate overhead statistics for each thread
    for thread, df in trace.threads:
        overhead = {
            "file": fname,
            "process": thread.parent.type,
            "thread": thread.type,
            "pid": thread.parent.pid,
            "tid": thread.tid,
        }

        # Collect wall-time for this thread
        # Time the last event finished minus time the first event started
        df_X = df[df.ph == "X"]
        overhead = overhead | {"wall_time": df_X.end.max() - df_X.ts.min()}

        # Compute scheduling overhead
        if thread.is_control or thread.is_worker:
            schd = df[df["name"] == SCHEDULER]
            overhead = overhead | {
                "scheduler": schd.dur.sum(),
                "overhead": schd.dur.sum(),
            }

        # Compute startup and shutdown overhead
        if thread.is_gate:
            esys = df[df.name == EVT_SYS]
            gthd = df[df.name == GAT_THD]
            assert len(esys) == 1 and len(gthd) == 1
            startup = gthd.ts.sum() - esys.ts.sum()
            shutdown = esys.end.sum() - gthd.end.sum()
            overhead = overhead | {
                "startup": startup,
                "shutdown": shutdown,
                "overhead": startup + shutdown,
            }

        # Collect statistics from event handler thread
        if thread.is_handler:
            handler = df[df.name == EVT_HDL]
            events = df[df.name.apply(lambda x: x in EVENTS)]
            assert len(handler) == 1
            idle_time = handler.dur.sum() - events.dur.sum()
            overhead = overhead | {
                "idle": idle_time,
                "overhead": idle_time,
            }

        records.append(overhead)

    return records


# Event Constants
# ==============================================================================

# Dictionary representing how the columns in the Event DataFrame should be
# renamed.
EVENT_COLUMN_NAME = {
    "file": "file",
    "pid": "pid",
    "arg_Origin": "rank_orig",
    "arg_Destination": "rank_dest",
    "arg_MPI_Tag": "mpi_tag",
    "name": "event",
    "ts": "ts",
    "end": "end",
}

# Dictionary representing the type each column in the Event DataFrame should be
# converted to.
EVENT_COLUMN_TYPE = {
    "file": "string",
    "pid": "int64",
    "rank_orig": "int32",
    "rank_dest": "int32",
    "mpi_tag": "int32",
    "event": "string",
    "ts": "float64",
    "end": "float64",
}

# Event Statistics
# ==============================================================================


def run_event_stats(files, options):
    """Run the event statistics analysis."""
    df = compute_events(files, metrics=options.metrics)

    # Select the intersetion of columns from the index and from the user-input
    cols_to_show = [
        column
        for column in df.columns.get_level_values(0).drop_duplicates()
        if column.casefold() in options.events
    ]

    if options.agg:
        # FIXME: Fix this pesky warning that happens during `agg`
        warnings.simplefilter(action="ignore", category=PerformanceWarning)
        df = df.reset_index().groupby("file").agg("sum").drop(columns=["pid"])

    if options.output is None:
        options.output = sys.stdout

    # Write data to output file
    pdu.save_dataframe(df[cols_to_show], file=options.output)


def compute_events(files, *, metrics):
    """Compute event statistics for all files."""
    dataframes = []

    for file in files:
        fname = os.path.basename(file.name.rstrip(".json"))
        trace = Trace.from_file(file).assign(file=fname)
        dataframes.append(trace)

    def get_timestamp(row):
        """Get start or end time depending on the type of event."""
        return row["ts"] if row["name"].endswith("/ Begin") else row["end"]

    def compute_elapsed_time(series):
        """Compute elapsed time between a pair of begin/end events."""
        assert len(series) == 2
        return series.max() - series.min()

    # Concat all DataFrames into a single one
    df = pd.concat(objs=dataframes)
    # Gather some metadata to be queried later
    meta = get_processes_metadata(df)
    # Select lines which correspond to runtime events
    mask = df.name.apply(lambda x: x.endswith("/ Begin") or x.endswith("/ End"))
    # Select which columns to drop
    cols_to_drop = list(df.columns.difference(EVENT_COLUMN_NAME.keys()))

    # Select only rows that correspond to runtime events
    df = df[mask]

    # Normalize DataFrame structure
    df = (
        # Rename columns to improve readability
        df.rename(columns=EVENT_COLUMN_NAME)
        # Fix the type of columns
        .astype(EVENT_COLUMN_TYPE)
        # Create two new columns:
        .assign(
            # Get timestamp of event: get start time if event is a begin or end
            # time otherwise
            time=df.apply(get_timestamp, axis=1),
            # Remove event tag suffix
            event=df.name.apply(lambda x: x.split(" / ")[0]),
        )
        # Drop unneeded columns
        .drop(columns=cols_to_drop + ["ts", "end"])
        # Group events by begin, end pairs
        .groupby(["file", "pid", "rank_orig", "rank_dest", "mpi_tag"])
        # Aggregate data by computing elapsed time between begin and end
        # counterparts
        .agg(
            {
                "event": "first",
                "time": compute_elapsed_time,
            }
        )
        # Reset old index to regular columns
        .reset_index()
    )

    # Format DataFrame before returning to the user
    df = (
        df.assign(
            # Create new column with process type (`Head` or `Worker`)
            process=df.apply(lambda x: meta[x["pid"]]["name"], axis=1),
            # Create new column with process index (order which they appear in
            # the trace)
            proc_index=df.apply(lambda x: meta[x["pid"]]["index"], axis=1),
        )
        # Group by event type (Alloc, Delete, Execute...)
        .groupby(["file", "pid", "process", "proc_index", "event"])
        # Aggregate events with user-provided metrics
        .agg(metrics)
        # Unstack "events" column as multiple columns (one for each event type)
        .unstack()["time"]
        # Show event type on top and metrics below
        .reorder_levels(order=[1, 0], axis="columns")
        # Sort columns after reordering levels
        .sort_index(axis="columns")
        # Sort index by file and process index
        .sort_index(level=["file", "proc_index"])
        # Remove process index column from DataFrame index
        .droplevel(level=["proc_index"])
        # Fill missing elements with zero
        .fillna(0)
    )

    return df


def get_processes_metadata(df):
    """Retrieve metadata information from the DataFrame."""
    meta = {}

    for pid, df in df.groupby("pid"):
        obj = {
            "name": df[df.name == "process_name"]["arg_name"].str.cat().split()[0],
            "index": int(df[df.name == "process_sort_index"]["arg_sort_index"].iloc[0]),
        }
        meta.update({pid: obj})

    return meta


def run_target_event_stats(timeline, output=sys.stdout):
    """
    Run the target event statistics analysis to extract duration metrics and
    event information.
    """

    stats = []
    events = timeline["traceEvents"]

    def filter_by_name(events, name, P=lambda _: True):
        """Filter events from a list according to their name and an optional
        predicate."""
        return list(evt for evt in events if evt["name"] == name and P(evt))

    def get_same_thread_event(events, push_event):
        """Given a push event, find the event in the same thread that is closest
        to it."""
        same_thread = [evt for evt in events if is_from_same_thread(push_event, evt)]
        return min(same_thread, key=lambda evt: (evt["ts"] - push_event["ts"]))

    def get_same_tag_event(events, push_event):
        """Given a push event, find the event with the same MPI tag and closest
        to it on the timeline."""
        same_tag = [
            evt
            for evt in events
            if evt["args"]["mpi_tag"] == push_event["args"]["mpi_tag"]
        ]
        return min(same_tag, key=lambda evt: (evt["ts"] - push_event["ts"]))

    # Target events contain source location information
    target_events = filter_by_name(events, "target")
    # Execute events contain duration metrics and target region index
    execute_events = filter_by_name(events, "Execute / Target Execution")
    # Push events contain the MPI tag of target events
    push_events = filter_by_name(
        events, "pushNewEvent", lambda evt: evt["args"]["event_type"] == "Execute"
    )

    # Sanity checks
    assert len(push_events) > 0
    assert len(push_events) == len(target_events)
    assert len(push_events) == len(execute_events)

    # For each push event, find the related event from the other lists. Then
    # create a new record with all the information necessary.
    for push_event in push_events:
        # Relate this push_event to other events
        target_evt = get_same_thread_event(target_events, push_event)
        execute_evt = get_same_tag_event(execute_events, push_event)
        # Create a new record with all information related to this task
        stats.append(
            {
                "region_index": execute_evt["args"]["target_region_index"],
                "duration": execute_evt["dur"],
                "sloc": target_evt["args"]["source_location"],
                "function": target_evt["args"]["identifier"],
                "outlined_fn_id": target_evt["args"]["outlined_fn_id"],
            }
        )

    # Create DataFrame and summarize statistics
    df = (
        pd.json_normalize(stats)
        .groupby(["sloc", "function", "outlined_fn_id"])
        .agg({"duration": ["mean", "min", "max", "count", "std"]})
    )

    # Rename columns and reset index
    df.columns = ["mean", "min", "max", "count", "std"]
    df = df.reset_index(level=[0, 1, 2]).set_index("outlined_fn_id")

    # Write data to output file
    pdu.save_dataframe(df, file=output)
