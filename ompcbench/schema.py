import os
import re

from itertools import product
from schema import Schema, And, Or, Use, Optional, Regex, SchemaError

from ompcbench.util import Map

# All supported dependency patterns in TaskBench
TASKBENCH_DEPS = [
    "all_to_all",
    "dom",
    "fft",
    "nearest",
    "no_comm",
    "random_nearest",
    "random_spread",
    "spread",
    "stencil_1d",
    "stencil_1d_periodic",
    "tree",
    "trivial",
]

# All supported kernel types in TaskBench
TASKBENCH_KERNELS = [
    "busy_wait",
    "compute_bound",
    "compute_bound2",
    "compute_dgemm",
    "empty",
    "io_bound",
    "load_imbalance",
    "memory_bound",
    "memory_daxpy",
]

# All implementations available in TaskBench
TASKBENCH_IMPLS = [
    "chapel",
    "charm++",
    "dask",
    "legion",
    "mpi",
    "mpi_openmp",
    "ompcluster",
    "ompss",
    "openmp",
    "parsec",
    "pygion",
    "realm",
    "regent",
    "spark",
    "starpu",
    "swift",
    "tensorflow",
    "x10",
]

# All routines available in Plasma
PLASMA_ROUTINE = (
    [
        "dzamax",
        "damax",
        "scamax",
        "samax",
        "zcposv",
        "dsposv",
        "zcgbsv",
        "dsgbsv",
        "zlag2c",
        "clag2z",
        "dlag2s",
        "slag2d",
    ]
    + list(
        map(
            "".join,
            product(
                ["z", "d", "c", "s"],
                [
                    "gbsv",
                    "gbtrf",
                    "geadd",
                    "gelqf",
                    "gelqs",
                    "gels",
                    "gemm",
                    "geqrf",
                    "geqrs",
                    "getri",
                    "getri_aux",
                    "lacpy",
                    "lange",
                    "lansy",
                    "lantr",
                    "lascl",
                    "laset",
                    "geswp",
                    "lauum",
                    "pbsv",
                    "pbtrf",
                    "langb",
                    "posv",
                    "poinv",
                    "potrf",
                    "potri",
                    "potrs",
                    "symm",
                    "syr2k",
                    "syrk",
                    "tradd",
                    "trmm",
                    "trsm",
                    "trtri",
                ],
            ),
        )
    )
    + list(
        map(
            "".join,
            product(
                ["z", "c"],
                [
                    "hemm",
                    "her2k",
                    "herk",
                    "hetrf",
                    "hesv",
                    "lanhe",
                    "unmlq",
                    "unmqr",
                ],
            ),
        )
    )
    + list(
        map(
            "".join,
            product(
                ["d", "s"],
                [
                    "sytrf",
                    "sysv",
                    "ormlq",
                    "ormqr",
                ],
            ),
        )
    )
)


class ExperimentSchema(Schema):
    def validate(self, data):
        data = super(ExperimentSchema, self).validate(data)
        # Ensures that exactly one was configured for the experiment
        tmp = []
        hasPlasma = data.experiment.plasma.samples
        hasTaskBench = data.experiment.taskbench.samples
        hasBlockMatMul = data.experiment.blockmatmul.samples
        tmp.append(hasPlasma)
        tmp.append(hasTaskBench)
        tmp.append(hasBlockMatMul)
        count = len(tmp) - tmp.count(None)
        if count == 0:
            raise SchemaError("No application was set for the experiment")
        if count > 1:
            raise SchemaError("Multiple applications were set for the experiment")
        # Adds defaults values to TaskBench schema after validation
        data.environment.all = data.environment.all or Map({})
        default = data.environment.all
        default.variables = default.variables or Map({})
        default.modules = default.modules or []
        if hasTaskBench:
            default.application.binary = default.application.binary or "ompcluster/main"
            default.application.flags = default.application.flags or []
        elif hasPlasma:
            default.application.binary = (
                default.application.binary or "build/plasmatest"
            )
            default.application.flags = default.application.flags or []
        if hasBlockMatMul:
            default.application.binary = default.application.binary or "block_matmul"
            default.application.flags = default.application.flags or []
        default.image = default.image or Map({})
        return data


# Schema expected from configuration file.
ConfigSchema = ExperimentSchema(
    And(
        {
            # --------------------------------------------------------------------------
            # REQUIRED SECTIONS
            # --------------------------------------------------------------------------
            # The version of OMPC Bench capable of interpreting this file
            "version": Regex(r"\d+(.\d+)+"),
            # Name of the experiment
            Optional("name", default="benchmark"): str,
            # Information used by Slurm laucher and Slurm script
            "slurm": And(
                {
                    # Slurm account
                    "account": str,
                    # Slurm partition
                    "partition": str,
                    # Slurm reservation
                    Optional("reservation", default=""): str,
                    # Job timelimit
                    Optional("timelimit", default="01:00:00"): Regex(
                        r"^\d\d:\d\d:\d\d$"
                    ),
                    # Slurm script verbosity (e.g. scripts/taskbench.slurm)
                    Optional("verbosity", default=0): And(int, lambda v: v >= 0),
                    # Additional flags to be passed to Slurm
                    Optional("flags", default=[]): [str],
                },
                Use(Map),
            ),
            # Section that properly specifies how an experiment should be carried
            "experiment": And(
                {
                    # Settings of experiment using TaskBench
                    Optional("taskbench", default=Map({})): And(
                        {
                            # Path to the repo on disk
                            "path": str,
                            # Application commit hash
                            # No version check should be performed if this is not specified.
                            Optional("hash", default=None): str,
                            # Matrix of options to be tested
                            "matrix": And(
                                {
                                    # Amount of nodes to allocate (including the head node)
                                    "nodes": [And(int, lambda n: n > 0)],
                                    # Which TaskBench kernels do test
                                    "kernel": [
                                        And(str, lambda k: k in TASKBENCH_KERNELS)
                                    ],
                                    # Size of the task graph in the form [width, steps]
                                    "size": [And([int], lambda k: len(k) == 2)],
                                    # How many iterations per task
                                    "iter": [And(int, lambda i: i > 0)],
                                    # Computation to communication ratio of the task
                                    "CCR": [And(float, lambda f: f > 0.0)],
                                    # Dependency type between tasks
                                    "dep": [
                                        And(str, lambda dep: dep in TASKBENCH_DEPS)
                                    ],
                                    # Strategies to use (user is free to choose names)
                                    "strat": [str],
                                },
                                Use(Map),
                            ),
                            # Amount of samples to collect per job
                            Optional("samples", default=1): And(int, lambda s: s >= 1),
                        },
                        Use(Map),
                    ),
                    # Settings of experiment using PLASMA
                    Optional("plasma", default=Map({})): And(
                        {
                            # Path to the repo on disk
                            "path": str,
                            # Application commit hash
                            # No version check should be performed if this is not specified.
                            Optional("hash", default=None): str,
                            # Matrix of options to be tested
                            "matrix": And(
                                {
                                    # Amount of nodes to allocate (including the head node)
                                    "nodes": [And(int, lambda n: n > 0)],
                                    # Which Plasma routine to test
                                    "routine": [
                                        And(str, lambda r: r in PLASMA_ROUTINE)
                                    ],
                                    # M x N x K dimensions, can be values or a range
                                    "dim": [
                                        Regex(
                                            r"(\d+)((:\d+){2})?(x(\d+)((:\d+){2})?){0,2}"
                                        )
                                    ],
                                    # N dimensions of matrix
                                    "nrhs": [Regex(r"(\d+)((:\d+){2})?")],
                                    # NB size of tile (NB by NB)
                                    "nb": [Regex(r"(\d+)((:\d+){2})?")],
                                    # NB inner size of tile (NB by NB)
                                    "nb_inner": [Regex(r"(\d+)((:\d+){2})?")],
                                    # Test the solution, yes or no
                                    "test": [Regex(r"[yn]")],
                                    # Strategies to use (user is free to choose names)
                                    "strat": [str],
                                },
                                Use(Map),
                            ),
                            # Amount of samples to collect per job
                            Optional("samples", default=1): And(int, lambda s: s >= 1),
                        },
                        Use(Map),
                    ),
                    # Settings of experiment using BlockMatMul
                    Optional("blockmatmul", default=Map({})): And(
                        {
                            # Path to the repo on disk
                            "path": str,
                            # Application commit hash
                            # No version check should be performed if this is not specified.
                            Optional("hash", default=None): str,
                            # Matrix of options to be tested
                            "matrix": And(
                                {
                                    # Amount of nodes to allocate (including the head node)
                                    "nodes": [And(int, lambda n: n > 0)],
                                    # Size of the task graph in the form [matrix_size, block_size]
                                    "size": [
                                        And(
                                            [int],
                                            lambda k: (
                                                len(k) == 2 and k[0] % k[1] == 0
                                            ),
                                        )
                                    ],
                                    # Strategies to use (user is free to choose names)
                                    "strat": [str],
                                },
                                Use(Map),
                            ),
                            # Amount of samples to collect per job
                            Optional("samples", default=1): And(int, lambda s: s >= 1),
                        },
                        Use(Map),
                    ),
                },
                Use(Map),
            ),
            # --------------------------------------------------------------------------
            # OPTIONAL SECTIONS
            # --------------------------------------------------------------------------
            # The environment of strategies
            Optional("environment", default=Map({})): And(
                {
                    # Name of some strategy
                    Optional(str): And(
                        {
                            # Environment variables that will be set for this strategy
                            Optional("variables", default=Map({})): dict,
                            # Environment Modules that will be loaded for this strategy
                            Optional("modules", default=[]): [str],
                            # Application settings (optional)
                            Optional("application", default=Map({})): And(
                                {
                                    # Application binary (relative to experiment.{type}.path)
                                    Optional("binary"): str,
                                    # Flags to be passed to TaskBench
                                    Optional("flags"): [str],
                                },
                                Use(Map),
                            ),
                            # Information about the container image
                            Optional("image", default=Map({})): And(
                                {
                                    # Path to the SIF file on disk
                                    "path": str,
                                    # OmpCluster commit hash
                                    # No version check should be performed if this is not specified.
                                    # Checked via `clang --version`
                                    Optional("ompc_hash"): str,
                                    # Flags to be passed to Singularity
                                    Optional("flags"): [str],
                                },
                                Use(Map),
                            ),
                        },
                        Use(Map),
                    ),
                },
                Use(Map),
            ),
        },
        Use(Map),
    )
)
