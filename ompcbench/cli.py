import os
import click
import yaml
import json
import orjson
import sys

from pathlib import Path
from contextlib import ExitStack

from ompcbench import __version__, messages as msg
from ompcbench.version import version_compatible
from ompcbench.util import Map
from ompcbench.schema import ConfigSchema
from ompcbench.experiment import Experiment
from ompcbench.analysis import run_analysis
from ompcbench.check import run_check
from ompcbench.merge import merge_traces
from ompcbench.critical_path import extract_ompc_critical_path
from ompcbench.filter import filter_ompc_traces
from ompcbench.statistics import (
    run_overhead_stats,
    run_event_stats,
    run_target_event_stats,
    EVENTS,
)
from ompcbench.metrics import extract_metrics_by_option
from ompcbench.lib.pandas_util import FORMATS

# GLOBAL CONSTANTS
# ------------------------------------------------------------------------------

# Available metrics for aggregating data, used for input validation.
AVAILABLE_METRICS = (
    "mean",
    "median",
    "mode",
    "std",
    "var",
    "min",
    "max",
    "count",
    "sum",
    "first",
    "last",
    "describe",
)

# COMMAND-LINE
# ------------------------------------------------------------------------------


@click.group()
@click.version_option(__version__)
@click.pass_context
def main(ctx):
    """
    A command-line benchmarking tool for OmpCluster with TaskBench.

    Launch a set of jobs with varying parameter ranges given a configuration
    file.
    """
    msg.secho(f"OMPC Bench, v{__version__}", bold=True)
    pass


@main.command()
@click.argument("CONFIGS", nargs=-1, required=True, type=click.File())
@click.option("-v", "--verbose", count=True, help="Enable verbose output.")
@click.option(
    "--verify/--no-verify",
    default=True,
    is_flag=True,
    help="Verify if paths are valid.",
)
@click.pass_obj
def check(ctx, configs, **options):
    """
    Check if a configuration file is valid.
    """
    options = Map(options)

    config = {}
    for element in configs:
        config.update(yaml.load(element, Loader=yaml.FullLoader) or {})

    try:
        run_check(config, options)
    except Exception as error:
        msg.fatal(error, traceback=True)


@main.command()
@click.argument("CONFIGS", nargs=-1, required=True, type=click.File())
@click.option("-o", "--output", type=click.Path(exists=False), help="Output directory.")
@click.option("-v", "--verbose", count=True, help="Enable verbose output.")
@click.option(
    "-n",
    "--dry-run",
    is_flag=True,
    help="Simulate job launch but do not modify anything.",
)
@click.option("-d", "--debug", is_flag=True, help="Enable debug mode.")
@click.option("-c", "--confirm", is_flag=True, help="Confirm before launching jobs.")
@click.option(
    "--verify/--no-verify",
    default=True,
    is_flag=True,
    help="Verify if paths are valid.",
)
@click.pass_obj
def run(ctx, configs, **options):
    """
    Launch a batch of jobs from a configuration file.
    """
    options = Map(options)
    if options.debug:
        msg.debug(f"Options: {options}")

    config = {}
    for element in configs:
        config.update(yaml.load(element, Loader=yaml.FullLoader) or {})

    if not ConfigSchema.is_valid(config):
        msg.fatal("Invalid config file. Run `check` to troubleshoot.")

    config = ConfigSchema.validate(config)

    if not version_compatible(config.version):
        msg.fatal("Incompatible version. Run `check` to troubleshoot.")

    experiment = Experiment(options, config)

    try:
        experiment.run()
    except click.exceptions.Abort:
        msg.warn("Program aborted.")
    except Exception as error:
        msg.fatal(error, traceback=True)


@main.command()
@click.argument(
    "DIRECTORY", nargs=1, required=True, type=click.Path(exists=True, file_okay=False)
)
@click.option("-v", "--verbose", count=True, help="Enable verbose output.")
@click.option("-o", "--output", type=click.File("w"), help="Output file.")
@click.option("-h", "--hide-empty", is_flag=True, help="Hide items without samples.")
@click.option(
    "-f",
    "--format",
    default=None,
    type=click.Choice(FORMATS.keys()),
)
@click.option(
    "-s",
    "--time-source",
    default="application",
    type=click.Choice(["shell", "application", "taskbench", "plasma", "blockmatmul"]),
)
@click.option("--raw", is_flag=True, help="Output unprocessed data.")
@click.option("--sep", default=",", type=str, help="CSV Separator.")
@click.option("--std/--no-std", default=None, is_flag=True, help="Display stddev.")
@click.option("--failure", is_flag=True, help="Display failure analysis.")
@click.option(
    "-b",
    "--bootstrap",
    type=float,
    default=None,
    help="Confidence level using bootstrap.",
)
@click.pass_obj
def analyze(ctx, directory, **options):
    """
    Analyze the result of an experiment.
    """
    options = Map(options)

    # Sanitize format
    # -----------------------------------------------------------

    default_format = "txt"

    if options.format is None:
        if options.output is not None:
            ext = os.path.splitext(options.output.name)[1][1:]
            options.format = ext if ext else default_format
        else:
            options.format = default_format

    # Sanitize bootstrap confidence level
    # ------------------------------------------------------------

    if options.bootstrap is not None:
        if not (0.0 < options.bootstrap < 1.0):
            msg.fatal(f"Invalid bootstrap confidence level {options.bootstrap}")

    # Sanitize standard deviation
    # ------------------------------------------------------------

    if options.std is None:
        if options.bootstrap is not None:
            msg.warn("Omitting stddev, you can enable it explicitly with `--std`.")
            options.std = False
        else:
            options.std = True

    try:
        run_analysis(directory, options)
    except Exception as error:
        msg.fatal(error, traceback=True)


@main.command()
@click.option(
    "-c",
    "--ompc-prefix",
    required=True,
    nargs=1,
    type=str,
    help="OMPC traces prefix.",
)
@click.option(
    "-t", "--ompt-prefix", nargs=1, type=str, help="OmpTracing traces prefix."
)
@click.option(
    "-o",
    "--output",
    default="tracing.json",
    type=click.File("wb"),
    help="Output file name.",
)
@click.option(
    "-d", "--developer", is_flag=True, help="Keep information meant for developers."
)
@click.option("--sync/--no-sync", default=True, help="Synchronize timeline.")
@click.pass_obj
def merge(ctx, ompc_prefix, ompt_prefix, output, **options):
    """
    Merge multiple JSON timelines from different processes into a single file.
    """
    options = Map(options)
    cwd = Path(".")

    with ExitStack() as stack:
        # List of all files that match prefix
        ompc_fnames = list(cwd.glob(f"{ompc_prefix}*.json"))
        ompt_fnames = list(cwd.glob(f"{ompt_prefix}*.json"))

        if len(ompc_fnames) == 0:
            msg.fatal(f"OMPC prefix `{ompc_prefix}` did not match any file.")

        if ompt_prefix and len(ompt_fnames) == 0:
            msg.warn(f"OMPT prefix `{ompt_prefix}` did not match any file.")

        # Open files and push to the stack to be cleanup automatically later
        ompc_files = [stack.enter_context(path.open(mode="r")) for path in ompc_fnames]
        ompt_files = [stack.enter_context(path.open(mode="r")) for path in ompt_fnames]

        # Get OMPC path to find the graph file
        ompc_path = Path(ompc_prefix)
        if not ompc_path.is_dir():
            ompc_path = Path(ompc_path.parent)

        try:
            timeline = merge_traces(
                ompc_path,
                ompc_files,
                ompt_files,
                developer=options.developer,
                synchronize=options.sync,
            )
            output.write(orjson.dumps(timeline))
        except Exception as error:
            msg.fatal(error, traceback=True)


@main.command()
@click.argument("DIR", nargs=1, required=True, type=click.Path(exists=True))
@click.option(
    "-o", "--output", nargs=1, default="critical_path.json", help="Output file name."
)
@click.pass_obj
def extract_critical_path(ctx, dir, **options):
    """
    Extract the critical path from different OMPCluster executions.
    """

    options = Map(options)

    try:
        extract_ompc_critical_path(dir, options.output)
    except Exception as error:
        msg.fatal(error, traceback=True)


@main.command()
@click.argument(
    "ompc-prefix",
    required=True,
    nargs=-1,
    type=str,
)
@click.option("-c", "--ompc", is_flag=True, help="Extract OMPC metrics.")
@click.option("-t", "--task", is_flag=True, help="Extract task metrics.")
@click.option(
    "-p", "--critical-path", is_flag=True, help="Extract critical path metrics."
)
@click.option(
    "-o", "--output", nargs=1, default="metrics.csv", help="Output file name."
)
@click.pass_obj
def extract_metrics(ctx, ompc_prefix, **options):
    """
    Extract metrics from different OMPC executions.
    """

    options = Map(options)
    cwd = Path(".")

    try:
        # Get metric option
        if options.ompc:
            metric_op = "ompc"
        elif options.task:
            metric_op = "task"
        else:
            metric_op = "critical-path"

        # Check if is a dir, file or prefix
        if len(ompc_prefix) == 1:
            root_dir = ompc_prefix[0]
            path = Path(root_dir)

            if path.exists():
                # Path is a directory
                if path.is_dir():
                    extract_metrics_by_option(
                        output=options.output, root_dir=root_dir, metric_op=metric_op
                    )
                # Path is a single trace file
                elif path.is_file():
                    root_dir = path.parent
                    extract_metrics_by_option(
                        output=options.output,
                        root_dir=root_dir,
                        trace_files=[root_dir],
                        metric_op=metric_op,
                    )
                else:
                    msg.fatal(
                        f"OMPC prefix `{root_dir}` did not match any file of directory."
                    )
            else:
                # Path is a prefix
                trace_files = list(cwd.glob(f"{root_dir}*.json"))
                root_dir = path.parent
                if len(trace_files) > 0:
                    extract_metrics_by_option(
                        output=options.output,
                        root_dir=root_dir,
                        trace_files=trace_files,
                        metric_op=metric_op,
                    )
                else:
                    msg.fatal(
                        f"OMPC prefix `{root_dir}` did not match any file of directory."
                    )
        else:
            root_dir = path.parent
            extract_metrics_by_option(
                output=options.output,
                root_dir=root_dir,
                trace_files=ompc_prefix,
                metric_op=metric_op,
            )

    except Exception as error:
        msg.fatal(error, traceback=True)


@main.command()
@click.option(
    "-c",
    "--ompc-prefix",
    required=True,
    nargs=1,
    type=str,
    help="OMPC traces directory.",
)
@click.option(
    "-o", "--output", nargs=1, default="tracing.json", help="Output file name."
)
@click.pass_obj
def filter(ctx, **options):
    """
    Filters and renames timeline events for the user.
    """
    options = Map(options)

    try:
        filter_ompc_traces(
            options.ompc_prefix,
            options.output,
        )
    except Exception as error:
        msg.fatal(error, traceback=True)


@main.group()
def stats():
    """Obtain statistics about a trace."""
    pass


@stats.command()
@click.argument("FILES", nargs=-1, required=True, type=click.File())
@click.option("-o", "--output", type=click.File("w"), help="Output file.")
@click.option(
    "-a", "--agg", is_flag=True, default=False, help="Aggregate data by file."
)
@click.option(
    "-c", "--cols", default=None, help="Command-separated list of columns to show."
)
@click.option(
    "-f",
    "--format",
    default=None,
    type=click.Choice(FORMATS.keys()),
)
@click.pass_obj
def overhead(ctx, files, **options):
    """Obtain statistics related to overhead."""
    options = Map(options)

    # Sanitize columns
    # -----------------------------------------------------------

    if options.cols is not None:
        options.cols = options.cols.split(",")

    # Sanitize format
    # -----------------------------------------------------------

    default_format = "txt"

    if options.format is None:
        if options.output is not None:
            ext = os.path.splitext(options.output.name)[1][1:]
            options.format = ext if ext else default_format
        else:
            options.format = default_format

    # Run overhead statistics
    # -----------------------------------------------------------

    try:
        run_overhead_stats(files, options)
    except Exception as error:
        msg.fatal(error, traceback=True)


@stats.command()
@click.argument("FILES", nargs=-1, required=True, type=click.File())
@click.option("-o", "--output", type=click.File("w"), help="Output file.")
@click.option(
    "-m",
    "--metrics",
    default="count,sum",
    help="Comma-separated list of metrics to compute.",
)
@click.option(
    "-e", "--events", default=None, help="Comma-separated list of events to show."
)
@click.option(
    "-a",
    "--agg",
    is_flag=True,
    default=False,
    help="Aggregate data by file using sum operation.",
)
@click.option(
    "-f",
    "--format",
    default=None,
    type=click.Choice(FORMATS.keys()),
)
@click.pass_obj
def events(ctx, files, **options):
    """Obtain statistics related to events."""
    options = Map(options)

    # Sanitize metrics
    # -----------------------------------------------------------

    options.metrics = options.metrics.lower().split(",")
    unknown_metrics = [
        metric for metric in options.metrics if metric not in AVAILABLE_METRICS
    ]

    if len(unknown_metrics) > 0:
        unknown = ", ".join(unknown_metrics)
        msg.fatal(f"Metrics `{unknown}` not recognized.")

    # Sanitize events
    # -----------------------------------------------------------

    default_events = [evt.casefold() for evt in EVENTS]

    if options.events is None:
        options.events = default_events
    else:
        options.events = options.events.casefold().split(",")

    unknown_events = [e for e in options.events if e not in default_events]

    if len(unknown_events) > 0:
        unknown = ", ".join(unknown_events)
        msg.fatal(f"Events `{unknown}` not recognized.")

    # Sanitize format
    # -----------------------------------------------------------

    default_format = "txt"

    if options.format is None:
        if options.output is not None:
            ext = os.path.splitext(options.output.name)[1][1:]
            options.format = ext if ext else default_format
        else:
            options.format = default_format

    # Run event statistics
    # -----------------------------------------------------------

    try:
        run_event_stats(files, options)
    except Exception as error:
        msg.fatal(error, traceback=True)


@stats.command()
@click.argument("FILES", nargs=-1, required=True, type=click.File("r"))
@click.option(
    "-o", "--output", default=sys.stdout, type=click.File("w"), help="Output file."
)
@click.pass_obj
def target_events(ctx, files, output):
    """Obtain statistics related to target executing events."""
    timeline = {}

    if len(files) > 1:
        timeline = merge_traces(files, [], developer=True, synchronize=False)
    else:
        timeline = json.load(files[0])

    # Run target executing statistics
    # -----------------------------------------------------------

    try:
        run_target_event_stats(timeline, output)
    except Exception as error:
        msg.fatal(error, traceback=True)


if __name__ == "__main__":
    main()
