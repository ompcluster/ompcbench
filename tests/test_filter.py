import os
import json

from ompcbench.filter import filter_ompc_traces

# ------------------------------------------------------------------------------
# Preparation

# TODO: The input test files are already "dissected", maybe we could add real
# JSON traces to make this test more robust.
INPUT_FILE = "tests/data/filter_input.json"
EXPECTED_FILE = "tests/data/filter_expected.json"
# TODO: Use temporary files instead of suffixes
OUTPUT_FILE = EXPECTED_FILE + ".output"

with open(EXPECTED_FILE, "r") as f:
    expected = json.load(f)

# User events filtered from trace
ALLOWED_EVENTS = [
    "Forward",
    "Alloc",
    "Execute",
    "Submit",
    "Delete",
    "Retrieve",
    "Forward / Begin",
    "Alloc / Begin",
    "Execute / Begin",
    "Submit / Begin",
    "Delete / Begin",
    "Retrieve / Begin",
    "Forward / End",
    "Alloc / End",
    "Execute / End",
    "Submit / End",
    "Delete / End",
    "Retrieve / End",
    "Target Nowait",
    "Target",
    "Target Enter Nowait",
    "Target Enter",
    "Target Exit Nowait",
    "Target Exit",
    "OMPC Runtime Execution",
]

# ------------------------------------------------------------------------------
# Actual tests


def test_filter_command():
    """Test filter command."""
    filter_ompc_traces(INPUT_FILE, OUTPUT_FILE)

    with open(OUTPUT_FILE, "r") as f:
        output = json.load(f)

    for evt in expected["traceEvents"]:
        if evt["name"] != "edge":
            assert evt in output["traceEvents"]

    assert len(output["traceEvents"]) == len(expected["traceEvents"])

    # Check if any events that should have been filtered out remain
    output_allowed_evts = []
    for evt in output["traceEvents"]:
        if evt["ph"] == "X" and evt["name"] != "edge" and "Task" not in evt["name"]:
            if not (len(evt["args"]) == 1 and "source_location" in evt["args"]):
                output_allowed_evts.append(evt)

    assert not list(
        [evt for evt in output_allowed_evts if evt["name"] not in ALLOWED_EVENTS]
    )

    # Count the number of non-M events
    output_without_M = [e for e in output["traceEvents"] if e["ph"] != "M"]
    expected_without_M = [e for e in expected["traceEvents"] if e["ph"] != "M"]

    assert len(output_without_M) == len(expected_without_M)
