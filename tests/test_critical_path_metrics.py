import pandas as pd

from ompcbench.critical_path_metrics import extract_critical_path_metrics

# ------------------------------------------------------------------------------
# Preparation

# JSON traces to extract metrics
INPUT_PATH = "tests/data/metrics_matmul/"

# Expected metric results
ALL_METRICS_EXPECTED_FILE = "tests/data/critical_path_expected.csv"
INDIVIDUAL_METRICS_EXPECTED_FILE = (
    "tests/data/critical_path_expected_metrics_matmul.csv"
)

# Metric results
ALL_METRICS_OUTPUT_FILE = ALL_METRICS_EXPECTED_FILE + ".output"
INDIVIDUAL_METRICS_OUTPUT_FILE = INDIVIDUAL_METRICS_EXPECTED_FILE + ".output"


# ------------------------------------------------------------------------------
# Metric names

ALL_METRICS_NAMES = [
    "Critical Path Time [ms]",
    "Max Critical Path Metric",
    "Min Critical Path Metric",
    "Average",
    "Median",
]
INDIVIDUAL_METRICS_NAMES = ["Load Balances", "Path Times"]


# ------------------------------------------------------------------------------
# Functions


def check_metric_format(metric):
    """
    Checks that format METRIC_COUNT x METRIC_VALUE is satisfied, with
    METRIC_COUNT as an integer and B as a positive decimal number. If satisfied,
    returns the METRIC_VALUE.
    """
    # Check if format METRIC_COUNT x METRIC_VALUE is satisfied
    metric_splitted = metric.split(" x ")
    assert len(metric_splitted) == 2
    metric_count, metric_value = metric_splitted

    # METRIC_COUNT must be greater than 0 and a integer number
    assert metric_count.isdigit()
    assert int(metric_count) > 0

    # Check if METRIC_VALUE is a positive float number
    metric_value_splitted = metric_value.split(".")
    assert len(metric_value_splitted) == 2
    metric_value_integer, metric_value_decimal = metric_value_splitted
    assert metric_value_integer.isdigit()
    assert metric_value_decimal.isdigit()

    return float(metric_value)


# ------------------------------------------------------------------------------
# Actual test


def test_extract_critiical_path_metrics_command():
    """
    Test if the extract critical path metrics command generates correct metric results.
    """
    # Extract metrics from OMPC traces
    extract_critical_path_metrics(output=ALL_METRICS_OUTPUT_FILE, root_dir=INPUT_PATH)

    # Read expected metric results
    all_metrics_expected_df = pd.read_csv(ALL_METRICS_EXPECTED_FILE)
    individual_metrics_expected_df = pd.read_csv(INDIVIDUAL_METRICS_EXPECTED_FILE)

    # Read metric results
    all_metrics_output_df = pd.read_csv(ALL_METRICS_OUTPUT_FILE)
    individual_metrics_output_df = pd.read_csv(INDIVIDUAL_METRICS_OUTPUT_FILE)

    # Check if results are as expected
    assert all_metrics_output_df.equals(all_metrics_expected_df)
    assert individual_metrics_output_df.equals(individual_metrics_expected_df)

    critical_path_time_value = 0

    # Check all metrics format and values
    for (
        critical_path_time,
        max_critical_path_metric,
        min_critical_path_metric,
        avg,
        median,
    ) in list(
        all_metrics_output_df[ALL_METRICS_NAMES].itertuples(index=False, name=None)
    ):
        # Check that average and median are valid values
        assert avg <= 1.0 and avg >= 0
        assert median <= 1.0 and median >= 0

        # Check the metrics format
        critical_path_time_value = check_metric_format(critical_path_time)
        max_critical_path_metric_value = check_metric_format(max_critical_path_metric)
        min_critical_path_metric_value = check_metric_format(min_critical_path_metric)

        # Check if max and min are valid values
        assert max_critical_path_metric_value >= min_critical_path_metric_value

    # Check individual metrics values
    critical_path_value = False
    for load_balance, path_time in list(
        individual_metrics_output_df[INDIVIDUAL_METRICS_NAMES].itertuples(
            index=False, name=None
        )
    ):
        assert load_balance <= 1.0 and load_balance >= 0
        if round(path_time, 2) == critical_path_time_value:
            critical_path_value = True
    # Check if critical path value is in path times
    assert critical_path_value
