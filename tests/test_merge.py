import os
import json

from pathlib import Path
from contextlib import ExitStack

from ompcbench.merge import merge_traces

# ------------------------------------------------------------------------------
# Preparation

INPUT_PREFIX = "tests/data/input"
EXPECTED_DEV_NAME = "tests/data/merged_developer.json"
EXPECTED_USR_NAME = "tests/data/merged_user.json"

# Load expected developer timeline after merging
with open(EXPECTED_DEV_NAME, "r") as f:
    expected_developer = json.load(f)

# Load expected user timeline after merging
with open(EXPECTED_USR_NAME, "r") as f:
    expected_user = json.load(f)

# ------------------------------------------------------------------------------
# Actual tests


def test_merge_command_developer():
    """Run merge command on test data and assert it is equal to expected."""
    output = {}

    with ExitStack() as stack:
        fnames = Path(".").glob(f"{INPUT_PREFIX}*.json")
        files = [stack.enter_context(fname.open(mode="r")) for fname in fnames]
        output = merge_traces(None, files, [], developer=True, synchronize=True)

    # They should have the same lenghts
    assert len(output["traceEvents"]) == len(expected_developer["traceEvents"])

    # All events in A should be in B
    # FIXME: Simply asserting A == B does not work. Is there a better way?
    for event in expected_developer["traceEvents"]:
        assert event in output["traceEvents"]

    # All events in B should be in A
    # FIXME: Simply asserting A == B does not work. Is there a better way?
    for event in output["traceEvents"]:
        assert event in expected_developer["traceEvents"]


import orjson


def test_merge_command_user():
    """Run merge command on test data and assert it is equal to expected."""
    output = {}

    with ExitStack() as stack:
        fnames = Path(".").glob(f"{INPUT_PREFIX}*.json")
        files = [stack.enter_context(fname.open(mode="r")) for fname in fnames]
        output = merge_traces(None, files, [], developer=False, synchronize=True)

    # They should have the same lenghts
    assert len(output["traceEvents"]) == len(expected_user["traceEvents"])

    exec_target_evts_expected = []
    exec_evts_expected = []

    # All events in A should be in B
    # FIXME: Simply asserting A == B does not work. Is there a better way?
    for event in expected_user["traceEvents"]:
        # Target Execution events change name to Task ID.
        if event["name"] == "Execute / Target Execution":
            exec_target_evts_expected.append(event)
        # Execute events have the new task_id attribute in the args.
        elif event["name"] == "Execute":
            exec_evts_expected.append(event)
        # Edge ids are provided by hash functions, so it will be different on
        # each execution.
        elif event["name"] != "edge":
            assert event in output["traceEvents"]

    task_evts = []
    exec_evts_output = []

    # All events in B should be in A
    # FIXME: Simply asserting A == B does not work. Is there a better way?
    for event in output["traceEvents"]:
        # Get Task and Execute events to compare len list.
        if "Task" in event["name"]:
            task_evts.append(event)
        elif event["name"] == "Execute":
            exec_evts_output.append(event)
        # Edge ids are provided by hash functions, so it will be different on
        # each execution.
        elif event["name"] != "edge":
            assert event in expected_user["traceEvents"]

    # Target Execution events have been renamed to Tasks ID events.
    # So the number of events of the lists must be the same.
    assert len(exec_target_evts_expected) == len(task_evts)

    # Since Execute events have a new task_id attribute, the events are not the
    # same. However, the number of events of the lists must be the same.
    assert len(exec_evts_expected) == len(exec_evts_output)
