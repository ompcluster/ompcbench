import pandas as pd

from ompcbench.ompc_metrics import extract_ompc_metrics

# ------------------------------------------------------------------------------
# Preparation

# JSON traces to extract metrics
INPUT_PATH = "tests/data/metrics_matmul/"

# Expected metric results
EXPECTED_FILE = "tests/data/ompc_metrics_matmul_expected.csv"

# Metric results
OUTPUT_FILE = EXPECTED_FILE + ".output"


# ------------------------------------------------------------------------------
# Metric names

TOTAL_TIME = "Total time [h:min:sec:ms]"
PARALLEL_TIME = "Parallel Time [h:min:sec:ms]"
COMPUTATION_TIME = "Computation Time [%]"
EFFICIENCY_METRICS = [
    "Computation Load Balance Efficiency",
    "Computation Efficiency",
    "Computation Parallel Efficiency",
    "Communication Load Balance Efficiency",
    "Communication Efficiency",
    "Communication Parallel Efficiency",
    "Process Load Balance Efficiency",
    "Process Communication Efficiency",
    "Process Efficiency",
    "Serial Region Efficiency",
]

# ------------------------------------------------------------------------------
# Convert time format


def get_time_ms(time):
    """
    Return the time in milliseconds, given the time in
    hours:minutes:seconds:milliseconds format.
    """

    # Time constants
    HOUR_MIN_MUL = 60
    SEC_MUL = 1000

    time_h, time_min, time_sec, time_ms = map(int, time.split(":"))

    # Convert hours to minutes
    time_min += time_h * HOUR_MIN_MUL

    # Convert minutes to seconds
    time_sec += time_min * HOUR_MIN_MUL

    # Convert seconds to milliseconds
    time_ms += time_sec * SEC_MUL

    return time_ms


# ------------------------------------------------------------------------------
# Actual test


def test_extract_ompc_metrics_command():
    """
    Test if the extract OMPC metrics command generates correct metric results.
    """
    # Extract metrics from OMPC traces
    extract_ompc_metrics(output=OUTPUT_FILE, root_dir=INPUT_PATH)

    # Read expected metric results
    expected_metrics_df = pd.read_csv(EXPECTED_FILE)

    # Read metric results
    output_metrics_df = pd.read_csv(OUTPUT_FILE)

    # Check if results are as expected
    assert output_metrics_df.equals(expected_metrics_df)

    # Total time is always less than parallel time
    for total_time, parallel_time in list(
        output_metrics_df[[TOTAL_TIME, PARALLEL_TIME]].itertuples(
            index=False, name=None
        )
    ):
        assert get_time_ms(total_time) < get_time_ms(parallel_time)

    # Computation time is a percentage, so its value is always between 0 and 100
    for computation_time in output_metrics_df[COMPUTATION_TIME]:
        assert computation_time >= 0 and computation_time <= 100

    # Efficiency metrics are always between 0 and 1
    for efficiency_metric in EFFICIENCY_METRICS:
        for efficiency_metric_result in output_metrics_df[efficiency_metric]:
            assert efficiency_metric_result >= 0 and efficiency_metric_result <= 1
