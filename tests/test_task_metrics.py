import pandas as pd

from ompcbench.task_metrics import (
    extract_task_metrics,
    EXEC_TASK,
)

# ------------------------------------------------------------------------------
# Preparation

# JSON traces to extract metrics
INPUT_PATH = "tests/data/metrics_matmul/"

# Expected metric results
PREFIX_NAME = "tests/data/task_metrics_matmul_expected"
EXPECTED_FILE = PREFIX_NAME + ".csv"

# Metric results
OUTPUT_FILE = EXPECTED_FILE + ".output"
OUTPUT_FILE_RESULT = PREFIX_NAME + "_0" + ".csv.output"


# ------------------------------------------------------------------------------
# Metric names
METRICS_NAMES = ["AVG", "MAX", "MIN", "SUM"]
METRICS_PERCENTAGE = ["AVG %", "MAX %", "MIN %"]
TYPE_METRIC = "Type"
SUM_PERCENTAGE_METRIC = "SUM %"

# ------------------------------------------------------------------------------
# Actual test


def test_extract_task_metrics_command():
    """
    Test if the extract task metrics command generates correct metric results.
    """
    # Extract metrics from OMPC traces
    extract_task_metrics(output=OUTPUT_FILE, root_dir=INPUT_PATH)

    # Read expected metric results
    expected_metrics_df = pd.read_csv(EXPECTED_FILE)

    # Read metric results
    output_metrics_df = pd.read_csv(OUTPUT_FILE_RESULT)

    # Check if results are as expected
    assert output_metrics_df.equals(expected_metrics_df)

    # All metrics percentage must be less than 100
    for metrics_percentages in list(
        output_metrics_df[METRICS_PERCENTAGE + [SUM_PERCENTAGE_METRIC]].itertuples(
            index=False, name=None
        )
    ):
        for metrics_percentage in metrics_percentages:
            assert metrics_percentage <= 100

    # All individual metrics must be less than SUM
    for avg, max, min, sum in list(
        output_metrics_df[METRICS_NAMES].itertuples(index=False, name=None)
    ):
        assert avg <= sum and max <= sum and min <= sum

    # The sum of the percentages of the metrics must add up to 100%
    task_metrics_sum = 0
    comm_metrics_sum = 0
    for sum, type_metric in list(
        output_metrics_df[[SUM_PERCENTAGE_METRIC, TYPE_METRIC]].itertuples(
            index=False, name=None
        )
    ):
        if type_metric == EXEC_TASK:
            task_metrics_sum += sum
        else:
            comm_metrics_sum += sum

    assert task_metrics_sum == 100
    assert comm_metrics_sum == 100
