import json

from ompcbench.critical_path import extract_ompc_critical_path
from ompcbench.util import get_lower_ts, get_finalization_time

# ------------------------------------------------------------------------------
# Preparation

# JSON traces to make this test more robust.
INPUT_PATH = "tests/data/critical_path/"
EXPECTED_FILE = "tests/data/critical_path_expected.json"
OUTPUT_FILE = EXPECTED_FILE + ".output"

with open(EXPECTED_FILE, "r") as f:
    expected = json.load(f)

# ------------------------------------------------------------------------------
# Actual test


def test_critical_path_command():
    """Test if the critical path command generates a correct timeline."""
    extract_ompc_critical_path(INPUT_PATH, OUTPUT_FILE)

    with open(OUTPUT_FILE, "r") as f:
        output = json.load(f)

    # Get expected task and edge events from critical path
    edge_evts_expc = [e for e in expected if e["name"] != "edge"]
    task_evts_expc = [e for e in expected if "Execute" in e["name"]]

    # Get task and edge events from critical path
    edge_evts = [e for e in output if e["name"] != "edge"]
    task_evts = [e for e in output if "Execute" in e["name"]]

    # Get metadata events to get a list of pids
    metadata_evts = [e for e in expected if e["ph"] == "M"]
    process_name_evts = [
        e for e in metadata_evts if e["args"]["name"] == "process_name"
    ]

    for process_name_evt in process_name_evts:
        pid = process_name_evt["pid"]

        # Get task and edge events from the same process
        edge_evts_same_pid = [e for e in edge_evts if e["pid"] == pid]
        task_evts_same_pid = [e for e in task_evts if e["pid"] == pid]

        # Get task and edge events expected from the same process
        edge_evts_expc_same_pid = [e for e in edge_evts_expc if e["pid"] == pid]
        task_evts_expc_same_pid = [e for e in task_evts_expc if e["pid"] == pid]

        # Check if events on the same critical path have the same number of tasks and dependencies
        assert len(edge_evts_same_pid) == len(edge_evts_expc_same_pid)
        assert len(task_evts_same_pid) == len(task_evts_expc_same_pid)

        # Check if the critical path has the same time
        critical_path_time = get_finalization_time(task_evts_same_pid) - get_lower_ts(
            task_evts_same_pid
        )
        critical_path_time_expc = get_finalization_time(
            task_evts_expc_same_pid
        ) - get_lower_ts(task_evts_expc_same_pid)
        assert critical_path_time == critical_path_time_expc
